import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'BackendManager.dart';
import 'CommServEvent.dart';
import 'CommServEventDay.dart';
import 'EventView.dart';
import 'UpdatingCalendarTodayWidget.dart';

class DayView extends StatefulWidget {
  final DateTime _dt;
  @override
  createState() => new DayViewState(_dt);
  DayView(this._dt, {Key key}) : super(key: key);
}

class DayViewState extends State<DayView> {
  static final DateFormat _headerFormatter = new DateFormat("MMMM d, y");
  static final DateFormat _columnFormatter = new DateFormat("jm");

  String _header;
  final DateTime initDT;
  static final epoch = new DateTime(1970, 1, 1);
  PageController _pageController;
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  DayViewState(this.initDT)
      : _pageController = new PageController(
          initialPage: eDaysFromDT(initDT),
          keepPage: false,
        ),
        _header = _headerFormatter.format(initDT);

  static int eDaysFromDT(DateTime dt) {
    return (dt.difference(epoch).inHours / 24).round();
  }

  static DateTime dtFromEDays(int eDays) {
    return epoch.add(new Duration(days: eDays));
  }

  @override
  Widget build(BuildContext context) {
    new BackendManager()
        .setContext(_scaffoldKey, _refreshIndicatorKey, setState);
    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        title: new Text(_header),
        actions: <Widget>[
          new UpdatingCalendarTodayWidget(
            setState,
            _pageController,
            eDaysFromDT(new DateTime(new DateTime.now().year,
                new DateTime.now().month, new DateTime.now().day)),
          ),
        ],
      ),
      body: _buildDayViewPageView(),
    );
  }

  Widget _buildDayViewPageView() {
    return new Scaffold(body: new Container(child: new LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
      return new RefreshIndicator(
          onRefresh: new BackendManager().onRefresh,
          key: _refreshIndicatorKey,
          child: new SingleChildScrollView(
              physics: new AlwaysScrollableScrollPhysics(),
              child: new Container(
                  height: constraints.maxHeight,
                  child: new PageView.builder(
                      controller: _pageController,
                      itemBuilder: (BuildContext context, int index) {
                        return _buildDayView(dtFromEDays(index));
                      },
                      onPageChanged: (int dayNum) {
                        setState(() {
                          _header =
                              _headerFormatter.format(dtFromEDays(dayNum));
                        });
                      }))));
    })));
  }

  Widget _buildDayView(DateTime day) {
    final eventWidgets = <Widget>[];

    final events = new BackendManager().getEventsOnDay(day);

    events.forEach((CommServEvent event, List<int> days) {
      days.forEach((int dayIndex) {
        eventWidgets.add(new InkWell(
            onTap: () async {
              //final result = await
              Navigator.push(
                  this.context,
                  new MaterialPageRoute(
                      builder: (context) => new EventView(event, dayIndex)));
              //_pageController.animateToPage(eDaysFromDT(result),
              //duration: UpdatingCalendarTodayWidget.duration,
              //curve: UpdatingCalendarTodayWidget.curve);
            },
            child: _createEventItem(event, dayIndex)));
      });
    });

    return new ListView(
      children: eventWidgets,
      physics:
          new ClampingScrollPhysics(), // holy fuck this was hard to find how to make work
      padding:
          new EdgeInsets.only(left: 16.0, right: 16.0, top: 8.0, bottom: 8.0),
    );
  }

  Widget _createEventItem(CommServEvent event, int index) {
    bool goingToEvent =
        new BackendManager().data.user.isGoing(event.id.toString(), index);

    CommServEventDay d = event.eventDays[index];

    return new ListTile(
        title: new Text(
          event.name.trim(),
          style: new TextStyle(
              fontWeight: goingToEvent ? FontWeight.bold : FontWeight.normal),
        ),
        subtitle: new Text(event.isHidden
            ? "${event.eventDays[index].effectiveDuration.inHours} Hours"
            : "${_columnFormatter.format(d.startTime)} – ${_columnFormatter.format(d.endTime)}"));
  }
}
