import 'package:json_annotation/json_annotation.dart';

import 'BackendManager.dart';
import 'CommServEvent.dart';
import 'CommServPerson.dart';

part "Data.g.dart";

@JsonSerializable(nullable: false)
class Data extends Object with _$DataSerializerMixin {
  List<CommServEvent> events = new List<CommServEvent>();
  List<CommServPerson> people = new List<CommServPerson>();
  Data({this.events, this.people});
  Data.empty();
  Data.event(CommServEvent e) {
    events = new List<CommServEvent>.from([e]);
    people = new List<CommServPerson>();
  }
  Data.person(CommServPerson p) {
    events = new List<CommServEvent>();
    people = new List<CommServPerson>.from([p]);
  }
  Data.ppl(List<CommServPerson> p) {
    events = new List<CommServEvent>();
    people = new List<CommServPerson>.from(p);
  }

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);

  bool isProfilePageReady() {
    for (int i = 0; i < people.length; i++) {
      if (people[i].id == new BackendManager().userID) {
        return true;
      }
    }
    return false;
  }

  Data resultOfApplying(Data newData) {
    return this.clone()..replaceWith(newData);
  }

  void replaceWith(Data newData) {
    removeEqual(newData);
    events.addAll(newData.events);
    people.addAll(newData.people);
  }

  void removeEqual(Data newData) {
    for (int i = 0; i < newData.events.length; i++) {
      events.removeWhere((CommServEvent e) => (e.id == newData.events[i].id));
    }
    for (int i = 0; i < newData.people.length; i++) {
      people.removeWhere((CommServPerson e) => (e.id == newData.people[i].id));
    }
  }

  CommServPerson get user {
    return people.firstWhere(
        (CommServPerson p) => p.id == new BackendManager().userID,
        orElse: () => null);
  }

  Map<CommServEvent, List<int>> get futureUserEvents {
    Map<CommServEvent, List<int>> ret = new Map<CommServEvent, List<int>>();

    for (int i = 0; i < events.length; i++) {
      final c = events[i].clone();
      for (int j = 0; j < c.eventDays.length; j++) {
        if (user.isGoing(c.id.toString(), j)) {
          if (c.eventDays[j].endTime.isAfter(new DateTime.now())&&
              !c.eventDays[j].deleted &&
              user.canSee(c)) {
            if (!ret.containsKey(c)) {
              ret[c] = new List<int>();
            }
            ret[c].add(j);
          }
        }
      }
    }

    return ret;
  }

  Map<CommServEvent, List<int>> get pastUserEvents {
    Map<CommServEvent, List<int>> ret = new Map<CommServEvent, List<int>>();

    for (int i = 0; i < events.length; i++) {
      final c = events[i].clone();
      for (int j = 0; j < c.eventDays.length; j++) {
        if (user.isGoing(c.id.toString(), j)) {
          if (c.eventDays[j].endTime.isBefore(new DateTime.now()) &&
              !c.eventDays[j].deleted &&
              user.canSee(c)) {
            if (!ret.containsKey(c)) {
              ret[c] = new List<int>();
            }
            ret[c].add(j);
          }
        }
      }
    }

    return ret;
  }

  int get userMinutes => user.getCumulativePastMinutes(events);

  Data clone({List<int> Nevents, List<int> Npeople}) {
    return new Data(
        events: Nevents ?? events.map((CommServEvent e) => e.clone()).toList(),
        people:
            Npeople ?? people.map((CommServPerson p) => p.clone()).toList());
  }
}
