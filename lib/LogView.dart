import 'dart:collection';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'BackendManager.dart';
import 'CommServEvent.dart';

enum EventType { past, future }

class LogViewScreen extends StatefulWidget {
  final EventType eventType;
  LogViewScreen(this.eventType, {Key key}) : super(key: key);
  @override
  createState() => new LogViewScreenState(eventType);
}

class LogViewScreenState extends State<LogViewScreen> {
  EventType eventType;
  LogViewScreenState(this.eventType);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text("Event Log"),
          //leading: new IconButton(
          //icon: new BackButtonIcon(),
          //onPressed: () => Navigator.pop(context)),
        ),
        body: new Builder(
            builder: (BuildContext context) =>
                LogViewState.buildLogView(context, eventType, 0)));
  }
}

class LogView extends StatefulWidget {
  final int count;
  final EventType eventType;
  LogView(this.count, this.eventType, {Key key}) : super(key: key);
  @override
  createState() => new LogViewState(count, eventType);
}

class LogViewState extends State<LogView> {
  int count;
  EventType eventType;

  static final tableColumnWidthMap = {
    0: FractionColumnWidth(0.2), //date
    1: FractionColumnWidth(0.4), //name
    2: FractionColumnWidth(0.2), //hrs
    3: FractionColumnWidth(0.2), //confirmed
  };

  LogViewState(this.count, this.eventType);

  @override
  Widget build(BuildContext context) {
    return buildLogView(context, eventType, count);
  }

  static Widget buildLogView(
      BuildContext context, EventType eventType, int count) {
    var events = getAppropriateEvents(eventType, count);

    var rows = new List<TableRow>();

    rows.add(new TableRow(children: [
      createWithPadding(new Text("Date",
          style: new TextStyle(decoration: TextDecoration.underline))),
      createWithPadding(new Text("Event Name",
          style: new TextStyle(decoration: TextDecoration.underline))),
      createWithPadding(new Text("Hours",
          style: new TextStyle(decoration: TextDecoration.underline))),
      createWithPadding(new Text("Confirmed",
          style: new TextStyle(decoration: TextDecoration.underline))),
    ]));

    events.forEach((CommServEvent e, List<int> indices) {
      indices.forEach((int indexy) {
        final day = e.eventDays[indexy];
        var tableRow = new TableRow(children: [
          new TableCell(
            child: new Padding(
              padding: new EdgeInsets.only(left: 4.0),
              child: new Text(new DateFormat("M/d").format(day.startTime)),
            ),
            verticalAlignment: TableCellVerticalAlignment.middle,
          ),
          new TableCell(
            child: new Text(e.name.trim()),
            verticalAlignment: TableCellVerticalAlignment.middle,
          ),
          new TableCell(
            child: new Text(
              day.effectiveDuration.inHours.toString() +
                  " hr" +
                  (day.effectiveDuration.inHours != 1 ? "s" : "") + //plurals
                  (e.leaders.contains(new BackendManager().data.user.id)
                      ? " x " +
                          new BackendManager().leaderTimeMultiplier.toString()
                      : ""),
            ),
            verticalAlignment: TableCellVerticalAlignment.middle,
          ),
          new TableCell(
            child: // TODO: not use inHours because it rounds down - ask bahr what the proper thing is
                new Checkbox(
                    value: new BackendManager()
                        .data
                        .user
                        .isConfirmed(e.id.toString(), indexy),
                    onChanged: null),
            verticalAlignment: TableCellVerticalAlignment.middle,
          ),
        ]);
        rows.add(tableRow);
      });
    });
    return new Table(
      children: rows,
      columnWidths: tableColumnWidthMap,
    );
  }

  static Widget createWithPadding(Widget thing) => new Padding(
        padding: EdgeInsets.only(left: 4.0, right: 4.0, top: 4.0, bottom: 4.0),
        child: thing,
      );

  static LinkedHashMap<CommServEvent, List<int>> getAppropriateEvents(
      EventType type, int count) {
    if (count == 0) {
      if (type == EventType.future) {
        return new BackendManager().data.futureUserEvents;
      }
      return new BackendManager().data.pastUserEvents;
    } else {
      var _events = (type == EventType.future)
          ? new BackendManager().data.futureUserEvents
          : new BackendManager().data.pastUserEvents;

      var _sortedEventKeys = List.from(_events.keys)
        ..sort((one, two) => one.eventDays[_events[one].reduce(min)].startTimeMS
            .compareTo(two.eventDays[_events[two].reduce(min)].startTimeMS));
      if (type == EventType.past) {
        _sortedEventKeys = List.from(_sortedEventKeys.reversed);
      }
      LinkedHashMap<CommServEvent, List<int>> _sortedEventMap =
          new LinkedHashMap.fromIterable(_sortedEventKeys,
              key: (key) => key, value: (key) => _events[key]);

      var _sem = new LinkedHashMap<CommServEvent, List<int>>();

      num start;
      num end;
      if (type == EventType.future) {
        start = 0;
        end = [count, _sortedEventMap.keys.length].reduce(min);
      } else {
        start = [_sortedEventMap.keys.length - count, 0].reduce(max);
        end = _sortedEventMap.keys.length;
      }
      for (var i = start; i < end; i++) {
        CommServEvent key = List.from(_sortedEventMap.keys)[i];
        _sem[key] = _sortedEventMap[key];
      }

      return _sem;
    }
  }
}
