import 'package:flutter/material.dart';
import 'dart:async';

class LifecycleEventHandler extends WidgetsBindingObserver {
  LifecycleEventHandler({this.resumeCallBack, this.suspendingCallBack});

  final Function resumeCallBack;
  final Function suspendingCallBack;

  @override
  Future<Null> didChangeAppLifecycleState(AppLifecycleState state) async {
    switch (state) {
      case AppLifecycleState.inactive:
      case AppLifecycleState.paused:
      case AppLifecycleState.suspending:
        if (suspendingCallBack != null) await suspendingCallBack();
        break;
      case AppLifecycleState.resumed:
        if (resumeCallBack != null) await resumeCallBack();
        break;
    }
  }
}
