import 'dart:async';

import 'package:flutter/material.dart';

import 'BackendManager.dart';

class SettingsView extends StatefulWidget {
  @override
  createState() => new SettingsViewState();
  SettingsView({Key key}) : super(key: key);
}

class SettingsViewState extends State<SettingsView> {
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    new BackendManager()
        .setContext(_scaffoldKey, _refreshIndicatorKey, setState);
    return new Scaffold(
        key: _scaffoldKey,
        appBar: new AppBar(
          title: new Text("Settings"),
        ),
        body: _buildSettingsView());
  }

  void _fullRefresh() async {
    Completer<BuildContext> comp = new Completer();
    bool success = false;
    Future.wait([
      new BackendManager().fullRefresh(),
    ])
      ..then((val) async {
        success = true;
        Navigator.of(await comp.future).pop();
      });

    while (!success) {
      await showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            comp.complete(context);
            return new AlertDialog(
                content: new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                  new CircularProgressIndicator(),
                ]));
          });
    }
  }

  Widget _buildSettingsView() {
    return new RefreshIndicator(
        child: new ListView(
          children: <Widget>[
            new ListTile(
              title: new Text("Full Refresh"),
              onTap: _fullRefresh,
            ),
            new ListTile(
                title: new Text("Logout"),
                onTap: () {
                  new BackendManager().logout();
                  _fullRefresh();
                }),
            new SwitchListTile(
                value: false,
                onChanged: null,
                title: new Text("Notifications"),
                subtitle: new Text("Coming Soon!")),
            new SwitchListTile(
                value: false,
                onChanged: null,
                title: new Text("Dark Theme"),
                subtitle: new Text("Coming Soon!")),
          ],
          padding: new EdgeInsets.only(
              left: 16.0, right: 16.0, top: 8.0, bottom: 8.0),
        ),
        key: _refreshIndicatorKey,
        onRefresh: new BackendManager().onRefresh);
  }
}
