import 'package:flutter/material.dart';

import 'BackendManager.dart';
import 'CommServPerson.dart';
import 'SelectUserView.dart';

class HelpView extends StatefulWidget {
  @override
  createState() => new HelpViewState();
  HelpView({Key key}) : super(key: key);
}

class HelpViewState extends State<HelpView> {
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    new BackendManager()
        .setContext(_scaffoldKey, _refreshIndicatorKey, setState);
    return new Scaffold(
        key: _scaffoldKey,
        appBar: new AppBar(
          title: new Text("Help"),
        ),
        body: _buildHelpView());
  }

  Widget _buildHelpView() {
    return new RefreshIndicator(
        child: new ListView(
          children: <Widget>[
            new ListTile(
              title: new Text("View Active Officers / Administrators"),
              onTap: () async {
                await Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new SelectUserView(
                            "View",
                            false,
                            new BackendManager()
                                .data
                                .people
                                .where(
                                    (CommServPerson p) => p.isPersonOfficer())
                                .toList()
                                  ..sort((p1, p2) =>
                                      p1.lastName.compareTo(p2.lastName)),
                            "officer",
                            "officers",
                            false)));
              },
            ),
            new Divider(),
            new ListTile(
                title: new Text("${BackendManager.APP_NAME}"),
                subtitle: new Text("Version ${BackendManager.APP_VERSION}")),
            new Text(
                "App made by Matthew Giordano, Sebastian Varma, and Ishan Kamat."),
          ],
          padding: new EdgeInsets.only(
              left: 16.0, right: 16.0, top: 8.0, bottom: 8.0),
        ),
        key: _refreshIndicatorKey,
        onRefresh: new BackendManager().onRefresh);
  }
}
