import 'package:flutter/material.dart';

class UpdatingCalendarTodayWidget extends StatelessWidget {
  static const Duration duration = const Duration(milliseconds: 500);
  static const Curve curve = Curves.ease;

  @override
  Widget build(BuildContext context) {
    return new Stack(
      children: <Widget>[
        new IconButton(
          icon: new Icon(Icons.calendar_today),
          onPressed: () => _pageController.animateToPage(_initialPage,
              duration: duration, curve: curve),
        ),
        new Padding(
          child: new Text(
            new DateTime.now().day.toString(),
            textScaleFactor: 0.8,
          ),
          padding: new EdgeInsets.only(top: 5.0),
        )
      ],
      alignment: Alignment.center,
    );
  }

  final Function _setStateFunc;
  final PageController _pageController;
  final int _initialPage;
  UpdatingCalendarTodayWidget(
      this._setStateFunc, this._pageController, this._initialPage);
}
