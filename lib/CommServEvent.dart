import 'package:json_annotation/json_annotation.dart';

import 'CommServEventDay.dart';
import 'CommServPerson.dart';
import 'Location.dart';

part "CommServEvent.g.dart";

@JsonSerializable(nullable: false)
class CommServEvent extends Object with _$CommServEventSerializerMixin {
  final int id;

  final String name;

  final String description;
  final List<CommServEventDay> eventDays;

  final Location parkingLocation;
  final String parkingInfo;

  final List<int> leaders;

  final int limit; //if this is -1 then it will be unlimited

  final bool isHidden;

  final List<String> files;

  CommServEvent({
    this.id,
    this.eventDays,
    this.name,
    this.description,
    this.parkingLocation,
    this.parkingInfo,
    this.leaders,
    this.isHidden,
    this.limit,
    this.files,
  });
  factory CommServEvent.fromJson(Map<String, dynamic> json) =>
      _$CommServEventFromJson(json);

  List<CommServPerson> getRoster(List<CommServPerson> allPeople, int dayIndex) {
    final ret = new List<CommServPerson>();
    for (int i = 0; i < allPeople.length; i++) {
      if (allPeople[i].isGoing(id.toString(), dayIndex)) {
        ret.add(allPeople[i]);
      }
    }
    return ret;
  }

  CommServEvent clone({
    int Nid,
    List<CommServEventDay> NeventDays,
    String Nname,
    String Ndescription,
    Location NparkingLocation,
    String NparkingInfo,
    List<int> Nleaders,
    bool NisHidden,
    int Nlimit,
    List<String> Nfiles,
  }) {
    return new CommServEvent(
      id: Nid ?? id,
      eventDays: NeventDays ?? new List.from(eventDays.map((f) => f.clone())),
      name: Nname ?? name,
      description: Ndescription ?? description,
      parkingLocation: NparkingLocation ?? parkingLocation.clone(),
      parkingInfo: NparkingInfo ?? parkingInfo,
      leaders: Nleaders ?? new List.from(leaders),
      isHidden: NisHidden ?? isHidden,
      limit: Nlimit ?? limit,
      files: Nfiles ?? new List.from(files),
    );
  }
}
