import 'dart:async';
import 'package:quiver/core.dart';

class Endpoint {
  String endpoint;
  String data;
  Endpoint(this.endpoint, this.data);
  operator ==(Object o) {
    return o is Endpoint && endpoint == o.endpoint && data == o.data;
  }
  int get hashCode => hash2(endpoint, data);

  Completer completer;

}
