import 'package:json_annotation/json_annotation.dart';

part 'CommServEventDay.g.dart';

@JsonSerializable(nullable: false)
class CommServEventDay extends Object with _$CommServEventDaySerializerMixin {
  final int startTimeMS;
  final int endTimeMS;
  final bool deleted;
  CommServEventDay({
    this.startTimeMS,
    this.endTimeMS,
    this.deleted,
  });
  factory CommServEventDay.fromJson(Map<String, dynamic> json) =>
      _$CommServEventDayFromJson(json);
  DateTime get startTime =>
      new DateTime.fromMillisecondsSinceEpoch(startTimeMS);
  DateTime get endTime => new DateTime.fromMillisecondsSinceEpoch(endTimeMS);
  Duration get _duration => endTime.difference(startTime);
  int get _minutes => _duration.inMinutes;

  int get effectiveMinutes {
    if (_minutes % 15 < 7.5) {
      return (_minutes ~/ 15) * 15;
    } else {
      return (_minutes ~/ 15) * 15 + 15;
    }
  }

  Duration get effectiveDuration => new Duration(minutes: effectiveMinutes);

  bool canSignUp() {
    final t = new DateTime.now();
    if (t.isBefore(endTime)) return true;
    if (!deleted &&
        t.year == endTime.year &&
        t.month == endTime.month &&
        t.day == endTime.day) return true;
    return false;
  }

  CommServEventDay clone({
    int NstartTimeMS,
    int NendTimeMS,
    bool Ndeleted,
  }) {
    return new CommServEventDay(
      startTimeMS: NstartTimeMS ?? startTimeMS,
      endTimeMS: NendTimeMS ?? endTimeMS,
      deleted: Ndeleted ?? deleted,
    );
  }

  CommServEventDay.dt(DateTime st, DateTime et)
      : this.startTimeMS = st.millisecondsSinceEpoch,
        this.endTimeMS = et.millisecondsSinceEpoch,
        this.deleted = false;

  @override
  bool operator ==(other) {
    return other is CommServEventDay &&
        other.startTimeMS == startTimeMS &&
        other.endTimeMS == endTimeMS &&
        other.deleted == deleted;
  }

  @override
  String toString() {
    return '{st: $startTime, et: $endTime, deleted: $deleted}';
  }
}
