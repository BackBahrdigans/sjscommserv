import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:map_view/map_view.dart' as mv;
import 'package:numberpicker/numberpicker.dart';

import 'BackendManager.dart';
import 'CommServEvent.dart';
import 'CommServEventDay.dart';
import 'CommServPerson.dart';
import 'CompositeSubscription.dart';
import 'Data.dart';
import 'NullContainer.dart';
import 'SelectUserView.dart';

class EditSummerEventView extends StatefulWidget {
  final CommServEvent _evt;
  @override
  createState() => new EditSummerEventViewState(_evt);
  EditSummerEventView(this._evt, {Key key}) : super(key: key);
}

class EditSummerEventViewState extends State<EditSummerEventView> {
  CommServEvent _evt;
  Data _ppl = new Data.empty();
  static final DateFormat _timeFormatter = new DateFormat("jm");
  static final DateFormat _dayFormatter = new DateFormat("EEEE MMMM d, y");
  mv.MapView mapView = new mv.MapView();
  var compositeSubscription = new CompositeSubscription();

  TextEditingController _nameController;
  TextEditingController _descController;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  EditSummerEventViewState(this._evt) {
    _nameController = new TextEditingController(text: _evt.name);
    _descController = new TextEditingController(text: _evt.description);
    final days = new List<CommServEventDay>();
    for (int i = 0; i < _evt.eventDays.length; i++) {
      final st = _evt.eventDays[i].startTime;
      final et = _evt.eventDays[i].endTime;
      days.add(_evt.eventDays[i].clone(
        NstartTimeMS:
            new DateTime(st.year, st.month, st.day, st.hour, st.minute)
                .millisecondsSinceEpoch,
        NendTimeMS: new DateTime(et.year, et.month, et.day, et.hour, et.minute)
            .millisecondsSinceEpoch,
      ));
    }
    _evt = _evt.clone(NeventDays: days);
  }

  void _save() async {
    _evt = _evt.clone(
      Nname: _nameController.text,
      Ndescription: _descController.text,
    );
    new BackendManager().updateDataUpdatesE(_evt);
    new BackendManager().updateDataUpdatesPPL(_ppl.people);

    final fullPeople = new BackendManager().data.people;
    final people = new List<CommServPerson>();
    for (int j = 0; j < _evt.eventDays.length; j++) {
      if (_evt.eventDays[j].deleted) {
        for (int i = 0; i < fullPeople.length; i++) {
          if (fullPeople[i].eventsSignedUpFor.containsKey(_evt.id.toString())) {
            if (fullPeople[i]
                .eventsSignedUpFor[_evt.id.toString()]
                .contains(j)) {
              people.add(fullPeople[i].clone()
                ..deConfirm(_evt.id.toString(), j)
                ..deRegister(_evt.id.toString(), j));
            }
          }
        }
      }
    }
    new BackendManager().updateDataUpdatesPPL(people);
    Navigator.pop(context, true);
  }

  @override
  Widget build(BuildContext context) {
    new BackendManager().setContext(_scaffoldKey, null, null);
    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        title: new Text("${_evt.id == -1 ? "Add" : "Edit"} Event ${_evt.name.trim()}"),
        actions: <Widget>[
          new IconButton(icon: new Icon(Icons.save), onPressed: _save),
        ],
      ),
      body: _buildEditSummerEventView(),
    );
  }

  Widget _buildEditSummerEventView() {
    return new ListView(
      padding:
          new EdgeInsets.only(left: 16.0, right: 16.0, top: 8.0, bottom: 8.0),
      children: <Widget>[
            new TextField(
              maxLines: 1,
              keyboardType: TextInputType.text,
              controller: _nameController,
              decoration: new InputDecoration(hintText: "Event Name"),
            ),
            new TextField(
              maxLines: null,
              keyboardType: TextInputType.multiline,
              controller: _descController,
              decoration: new InputDecoration(hintText: "Event Description"),
            ),
            new ListTile(
              leading: const Icon(Icons.calendar_today),
              title:
                  new Text(_dayFormatter.format(_evt.eventDays[0].startTime)),
              subtitle: new Text("Event Day"),
              onTap: () async {
                final n = await showDatePicker(
                    context: context,
                    initialDate: _evt.eventDays[0].startTime,
                    firstDate: new DateTime(new DateTime.now().year - 5),
                    lastDate: new DateTime(new DateTime.now().year + 5));
                if (n == null) return;
                final st = _evt.eventDays[0].startTime;
                final mod = n.difference(st).inMilliseconds;
                setState(() => _evt = _evt.clone(NeventDays: [
                      _evt.eventDays[0].clone(
                        NstartTimeMS: _evt.eventDays[0].startTimeMS + mod,
                        NendTimeMS: _evt.eventDays[0].endTimeMS + mod,
                      )
                    ]));
              },
            ),
            new Row(children: <Widget>[
              new Expanded(
                child: new Text(
                    "${_evt.eventDays[0].effectiveDuration.inHours} Hours, ${_evt.eventDays[0].effectiveMinutes % 60} Minutes"),
              ),
              new IconButton(
                  icon: new Icon(Icons.edit), onPressed: _showDurationPicker),
            ]),
            new Divider(),
          ] +
          _getPeople() +
          <Widget>[
            _evt.id == -1 ? new NullContainer() : new Divider(),
            _evt.id == -1
                ? new NullContainer()
                : new FlatButton(
                    onPressed: () {
                      showDialog(
                        context: context,
                        barrierDismissible: false,
                        builder: (newContext) {
                          return new AlertDialog(
                            content: new Text(
                                "Are you sure you want to delete ${_evt.name.trim()}?"),
                            actions: <Widget>[
                              new FlatButton(
                                  child: Text('Cancel'),
                                  onPressed: () {
                                    Navigator.of(newContext).pop();
                                  }),
                              new FlatButton(
                                  child: new Text("Delete"),
                                  onPressed: () {
                                    Navigator.of(newContext).pop();
                                    _evt = _evt.clone(
                                        NeventDays: _evt.eventDays
                                            .map((CommServEventDay d) =>
                                                d.clone(Ndeleted: true))
                                            .toList());
                                    new BackendManager()
                                        .updateDataUpdatesE(_evt);
                                    _save();
                                  }),
                            ],
                          );
                        },
                      );
                    },
                    child: new Text(
                      "Delete Event",
                      style: new TextStyle(color: Colors.red),
                    )),
          ],
    );
  }

  void _showDurationPicker() async {
    await showDialog<int>(
        context: context,
        builder: (BuildContext context) {
          return new NumberPickerDialog.integer(
            minValue: 0,
            maxValue: 9999,
            title: new Text("Hours"),
            initialIntegerValue: _evt.eventDays[0].effectiveDuration.inHours,
          );
        }).then((int value) {
      if (value != null) {
        setState(() {
          _evt.eventDays[0] = _evt.eventDays[0].clone(
              NendTimeMS: _evt.eventDays[0].startTime
                  .add(new Duration(
                      hours: value,
                      minutes: _evt.eventDays[0].effectiveMinutes % 60))
                  .millisecondsSinceEpoch);
        });
      }
    });
    await showDialog<int>(
        context: context,
        builder: (BuildContext context) {
          return new NumberPickerDialog.integer(
            minValue: 0,
            maxValue: 45,
            title: new Text("Minutes"),
            initialIntegerValue:
                _evt.eventDays[0].effectiveDuration.inMinutes % 60,
            //step: 15,
          );
        }).then((int value) {
      if (value != null) {
        setState(() {
          _evt.eventDays[0] = _evt.eventDays[0].clone(
              NendTimeMS: _evt.eventDays[0].startTime
                  .add(new Duration(
                      minutes: value,
                      hours: _evt.eventDays[0].effectiveDuration.inHours))
                  .millisecondsSinceEpoch);
        });
      }
    });
  }

  List<Widget> _getPeople() {
    List<CommServPerson> roster = _evt.getRoster(
        new BackendManager().data.resultOfApplying(_ppl).people, 0)
      ..sort((p1, p2) => p1.lastName.compareTo(p2.lastName));
    List<Widget> ret = <Widget>[
      new Row(children: <Widget>[
        new Expanded(
          child: new Text("Roster"),
        ),
        new IconButton(
          icon: new Icon(Icons.add),
          onPressed: () async {
            List<CommServPerson> newRoster = new BackendManager()
                .data
                .people // TODO: make sure everything is always alphabetically sorted
                .where(
                    (p) => !roster.map((p2) => p2.id).toList().contains(p.id))
                .toList();
            final int successIndex = await Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (context) => new SelectUserView(
                        "Select", true, newRoster, "user", "users", true)));
            if (successIndex == null) return;

            setState(
                () => _ppl.replaceWith(new Data.person(newRoster[successIndex]
                  ..register(_evt.id.toString(), 0)
                  ..confirm(_evt.id.toString(), 0))));
          },
        ),
      ])
    ];
    for (int i = 0; i < roster.length; i++) {
      ret.add(new Row(children: <Widget>[
        new Expanded(
          child: new Text("${roster[i].name} (${roster[i].email})"),
        ),
        new IconButton(
          icon: new Icon(Icons.delete),
          onPressed: () async {
            setState(() => _ppl.replaceWith(new Data.person(roster[i]
              ..deConfirm(_evt.id.toString(), 0)
              ..deRegister(_evt.id.toString(), 0))));
          },
        ),
      ]));
    }
    return ret;
  }
}
