import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import 'BackendManager.dart';
import 'CommServEvent.dart';
import 'CommServEventDay.dart';
import 'CommServPerson.dart';
import 'EditEventView.dart';
import 'EditSummerEventView.dart';
import 'NullContainer.dart';
import 'ParkingView.dart';
import 'RosterView.dart';
import 'SelectUserView.dart';

class EventView extends StatefulWidget {
  final CommServEvent _evt;
  final int _dayIndex;
  @override
  createState() => new EventViewState(_evt, _dayIndex);
  EventView(this._evt, this._dayIndex, {Key key}) : super(key: key);
}

class EventViewState extends State<EventView> {
  //CommServEvent get _evt => new BackendManager()
  //.data
  //.events
  //.firstWhere((CommServEvent e) => e.id == _evtID);
  CommServEvent _evt;
  int _dayIndex;
  CommServEventDay get _day => _evt.eventDays[_dayIndex];
  static final DateFormat _timeFormatter = new DateFormat("jm");
  static final DateFormat _dayFormatter = new DateFormat("EEEE MMMM d, y");
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();

  bool _superUser;
  _getSnackbarFail(String who) =>
      new SnackBar(content: Text('$who is editing.'));

  EventViewState(this._evt, this._dayIndex) {
    _superUser = new BackendManager().data.user.isSuperUserOfEvent(_evt);
  }

  @override
  void setState(Function f) {
    super.setState(() {
      f();
      try {
        _evt =
            new BackendManager().data.events.firstWhere((e) => e.id == _evt.id);
      } catch (e) {}
      _superUser = new BackendManager().data.user.isSuperUserOfEvent(_evt);
    });
  }

  final _scaffKey = new GlobalKey<ScaffoldState>();

  void _pressEditButton() async {
    bool shouldOpen = true;
    Completer<BuildContext> comp = new Completer();
    bool dontClose = true;
    Future.wait([
      new BackendManager().getLock(_evt.id),
      new BackendManager().onRefresh()
    ]).then((List<dynamic> successes) async {
      if (shouldOpen != true) return;
      try {
        Navigator.of(await comp.future).pop();
      } catch (p) {}
      if (successes[0]['result'] == true) {
        setState(() {
          _evt = new BackendManager()
              .data
              .events
              .firstWhere((CommServEvent e) => e.id == _evt.id)
              .clone();
        });

        if (_evt.isHidden) {
          await Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (context) => new EditSummerEventView(_evt.clone())));
        } else {
          await Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (context) => new EditEventView(_evt.clone())));
        }

        setState(() {
          _evt = new BackendManager()
              .data
              .events
              .firstWhere((CommServEvent e) => e.id == _evt.id)
              .clone();
        });
        Completer comp2 = new Completer();
        Future
            .wait([
              new BackendManager().onRefresh(),
            ])
            .then((val) => new BackendManager().releaseLock(_evt.id))
            .then((val) async {
              dontClose = false;
              Navigator.of(await comp2.future).pop();

              setState(() {
                _evt = new BackendManager()
                    .data
                    .events
                    .firstWhere((CommServEvent e) => e.id == _evt.id)
                    .clone();
              });

              if (new BackendManager().latestFailedUpdates.events.length != 0 ||
                  new BackendManager().latestFailedUpdates.people.length != 0) {
                return _pressEditButton();
              }

              bool deleted = true;
              for (int i = 0; i < _evt.eventDays.length; i++) {
                if (!_evt.eventDays[i].deleted) {
                  deleted = false;
                  break;
                }
              }
              if (deleted == true) {
                Navigator.of(context).pop();
              }
            });

        while (dontClose) {
          await showDialog(
              context: context,
              barrierDismissible: false,
              builder: (context) {
                comp2.complete(context);
                return new AlertDialog(
                    content: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                      new CircularProgressIndicator(),
                    ]));
              });
        }

        //if (_evt.eventDays.length < _dayIndex) {
        //_dayIndex = _evt.eventDays.length - 1;
        //}
        //if (result != null) {
        //Navigator.pop(context, result);
        //}
      } else if (successes[0] == null) {
      } else {
        _scaffKey.currentState
            .showSnackBar(_getSnackbarFail(successes[0]['person']));
        setState(() {});
      }
    });

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          comp.complete(context);
          return new AlertDialog(
              content: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                //new Expanded(
                //child:
                new CircularProgressIndicator(),
                //),
                new FlatButton(
                    child: Text('Cancel'),
                    onPressed: () {
                      Navigator.of(context).pop();
                      shouldOpen = false;
                    }),
              ]));
        });
  }

  @override
  Widget build(BuildContext context) {
    new BackendManager().setContext(_scaffKey, _refreshIndicatorKey, setState);
    return new Scaffold(
        key: _scaffKey,
        appBar: new AppBar(
          title: new Text(_evt.name.trim()),
          actions: <Widget>[
            _superUser
                ? new IconButton(
                    icon: new Icon(Icons.edit),
                    onPressed: _pressEditButton,
                  )
                : new NullContainer()
          ],
        ),
        body: _buildEventView());
  }

  Widget _buildEventView() {
    return new RefreshIndicator(
      child: new ListView(
          children: <Widget>[
                new ListTile(
                    leading: const Icon(Icons.title),
                    title: new Text(_evt.name.trim())),
                new Divider(),
                new ListTile(
                    leading: const Icon(Icons.text_fields),
                    title: new Text(_evt.description.trim())),
                new ListTile(
                  leading: const Icon(Icons.timer),
                  title: new Text(_evt.isHidden
                      ? "${_day.effectiveDuration.inHours} Hours"
                      : "${_timeFormatter.format(_day.startTime)} – ${_timeFormatter.format(_day.endTime)}"),
                  subtitle: new Text(_dayFormatter.format(_day.startTime)),
                ),
                new Divider(),
                new ListTile(
                  leading: const Icon(Icons.people),
                  title: new Text(
                      "Roster (${_evt.getRoster(new BackendManager().data.people, _dayIndex).length}${_evt.limit == -1 ? "" : " / ${_evt.limit}"})"),
                  trailing: const Icon(Icons.keyboard_arrow_right),
                  onTap: () {
                    Navigator.push(
                        this.context,
                        new MaterialPageRoute(
                            builder: (context) => (_evt.isHidden
                                ? new SelectUserView(
                                    "View",
                                    false,
                                    _evt.getRoster(
                                        new BackendManager().data.people,
                                        _dayIndex),
                                    "roster",
                                    "roster",
                                    false)
                                : new RosterView(_evt, _dayIndex))));
                  },
                ),
              ] +
              (_evt.isHidden
                  ? <Widget>[]
                  : (<Widget>[
                        new ListTile(
                          leading: const Icon(Icons.directions_car),
                          title: new Text("Parking Info"),
                          trailing: const Icon(Icons.keyboard_arrow_right),
                          onTap: () {
                            Navigator.push(
                                this.context,
                                new MaterialPageRoute(
                                    builder: (context) =>
                                        new ParkingView(_evt.id)));
                          },
                        ),
                      ] +
                      _getFormsWidgets() +
                      <Widget>[
                        new Padding(
                          padding: EdgeInsets.only(left: 16.0, right: 16.0),
                          child: _getSignUpButton(),
                        )
                      ]))),
      key: _refreshIndicatorKey,
      onRefresh: new BackendManager().onRefresh,
    );
  }

  Widget _getSignUpButton() {
    if (new BackendManager().data.user.isGoing(_evt.id.toString(), _dayIndex)) {
      return _day.canSignUp()
          ? new RaisedButton(
              child: new Text("Cancel"),
              color: Colors.red,
              textColor: Colors.white,
              onPressed: () async {
                CommServPerson p = new BackendManager().data.user
                  ..deRegister(_evt.id.toString(), _dayIndex);
                setState(() => new BackendManager().updateDataUpdatesP(p));
                new BackendManager().onRefresh();
              })
          : (new BackendManager()
                  .data
                  .user
                  .isConfirmed(_evt.id.toString(), _dayIndex)
              ? new RaisedButton(
                  child: new Text("Attended"),
                  color: Colors.green,
                  textColor: Colors.white,
                  onPressed: null,
                )
              : new RaisedButton(
                  child: new Text("Failed to Attended"),
                  color: Colors.red,
                  textColor: Colors.white,
                  onPressed: null,
                ));
    } else {
      return _day.canSignUp()
          ? (_evt.limit == -1 ||
                  _evt
                          .getRoster(
                              new BackendManager().data.people, _dayIndex)
                          .length <
                      _evt.limit
              ? new RaisedButton(
                  child: new Text("Sign Up"),
                  color: Colors.green,
                  textColor: Colors.white,
                  onPressed: () async {
                    CommServPerson p = new BackendManager().data.user
                      ..register(_evt.id.toString(), _dayIndex);
                    setState(() => new BackendManager().updateDataUpdatesP(p));
                    new BackendManager().onRefresh();
                  })
              : new RaisedButton(
                  child: new Text("At Maximum Capacity"), onPressed: null))
          : new NullContainer(); // TODO: say "Unregistered? or something"
    }
  }

  List<Widget> _getFormsWidgets() {
    if (_evt.files.length == 0) return <Widget>[];
    final List<Widget> forms = new List<Widget>();
    for (int i = 0; i < _evt.files.length; i++) {
      forms.add(new ListTile(
        title: new Text(_evt.files[i]),
        subtitle: new Text("Form ${i+1}"),
        onTap: () async {
          final url = _evt.files[i];
          if (await canLaunch(url)) {
            await launch(url);
          } else {
            _scaffKey.currentState
                .showSnackBar(new SnackBar(content: new Text("Invalid URL.")));
          }
        },
      ));
    }
    return <Widget>[
          new ListTile(
            leading: const Icon(Icons.edit),
            title: new Text("Forms"),
          )
        ] +
        forms;
  }
}
