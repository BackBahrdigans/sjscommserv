import 'package:flutter/material.dart';

import 'BackendManager.dart';
import 'CommServEvent.dart';
import 'CommServPerson.dart';
import 'SelectUserView.dart';

enum SelectChoice {
  all,
  none,
  invert,
  promote,
  demote,
  add,
  remove,
  emailAttendees,
  emailLeaders,
}

class RosterView extends StatefulWidget {
  final CommServEvent _evt;
  final int _dayIndex;
  @override
  createState() => new RosterViewState(_evt, _dayIndex);
  RosterView(this._evt, this._dayIndex, {Key key}) : super(key: key);
}

class RosterViewState extends State<RosterView> {
  //CommServEvent get _evt => new BackendManager()
  //.data
  //.events
  //.firstWhere((CommServEvent e) => e.id == _evtID);
  CommServEvent _evt;
  int _dayIndex;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _superUser;
  RosterViewState(this._evt, this._dayIndex) {
    _superUser = new BackendManager().data.user.isSuperUserOfEvent(_evt);
  }

  @override
  void setState(Function f) {
    super.setState(() {
      // TODO: why isnt this called on a refresh :(
      f();
      try {
        _evt =
            new BackendManager().data.events.firstWhere((e) => e.id == _evt.id);
      } catch (e) {}
      _superUser = new BackendManager().data.user.isSuperUserOfEvent(_evt);
    });
  }

  @override
  Widget build(BuildContext context) {
    new BackendManager()
        .setContext(_scaffoldKey, _refreshIndicatorKey, setState);
    return new Scaffold(
        key: _scaffoldKey,
        appBar:
            new AppBar(title: new Text("${_evt.name.trim()} Roster"), actions: <
                Widget>[
          PopupMenuButton<SelectChoice>(
              onSelected: (SelectChoice result) async {
                List<CommServPerson> roster =
                    _evt.getRoster(new BackendManager().data.people, _dayIndex);
                List<CommServPerson> updatedRoster = new List<CommServPerson>();
                switch (result) {
                  case SelectChoice.all:
                    for (int i = 0; i < roster.length; i++) {
                      if (!roster[i]
                          .isConfirmed(_evt.id.toString(), _dayIndex)) {
                        updatedRoster.add(roster[i].clone()
                          ..confirm(_evt.id.toString(), _dayIndex));
                      }
                    }
                    break;
                  case SelectChoice.none:
                    for (int i = 0; i < roster.length; i++) {
                      if (roster[i]
                          .isConfirmed(_evt.id.toString(), _dayIndex)) {
                        updatedRoster.add(roster[i].clone()
                          ..deConfirm(_evt.id.toString(), _dayIndex));
                      }
                    }
                    break;
                  case SelectChoice.invert:
                    for (int i = 0; i < roster.length; i++) {
                      if (!roster[i]
                          .isConfirmed(_evt.id.toString(), _dayIndex)) {
                        updatedRoster.add(roster[i].clone()
                          ..confirm(_evt.id.toString(), _dayIndex));
                      } else {
                        updatedRoster.add(roster[i].clone()
                          ..deConfirm(_evt.id.toString(), _dayIndex));
                      }
                    }
                    break;
                  case SelectChoice.promote:
                    final List<CommServPerson> noLeaders = roster
                        .where(
                            (CommServPerson p) => !_evt.leaders.contains(p.id))
                        .toList()
                          ..sort(
                              (p1, p2) => p1.lastName.compareTo(p2.lastName));
                    final int successIndex = await Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new SelectUserView("promote",
                                true, noLeaders, "user", "users", true)));
                    if (successIndex == null) return;

                    new BackendManager().updateDataUpdatesE(
                        _evt..leaders.add(noLeaders[successIndex].id));

                    break;
                  case SelectChoice.demote:
                    final List<CommServPerson> leaders = roster
                        .where(
                            (CommServPerson p) => _evt.leaders.contains(p.id))
                        .toList()
                          ..sort(
                              (p1, p2) => p1.lastName.compareTo(p2.lastName));
                    final int successIndex = await Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new SelectUserView("demote",
                                true, leaders, "leader", "leaders", true)));
                    if (successIndex == null) return;

                    new BackendManager().updateDataUpdatesE(
                        _evt..leaders.remove(leaders[successIndex].id));

                    break;
                  case SelectChoice.add:
                    final List<CommServPerson> notGoing = new BackendManager()
                        .data
                        .people
                        .where((CommServPerson p) => !roster
                            .map((CommServPerson p2) => p2.id)
                            .toList()
                            .contains(p.id))
                        .toList()
                          ..sort(
                              (p1, p2) => p1.lastName.compareTo(p2.lastName));
                    final int successIndex = await Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new SelectUserView(
                                "add", true, notGoing, "user", "users", true)));
                    if (successIndex == null) return;

                    new BackendManager().updateDataUpdatesP(
                        notGoing[successIndex]
                          ..register(_evt.id.toString(), _dayIndex));

                    break;
                  case SelectChoice.remove:
                    final int successIndex = await Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new SelectUserView(
                                "remove",
                                true,
                                roster
                                  ..sort((p1, p2) =>
                                      p1.lastName.compareTo(p2.lastName)),
                                "user",
                                "users",
                                true)));
                    if (successIndex == null) return;

                    new BackendManager().updateDataUpdatesP(roster[successIndex]
                      ..deRegister(_evt.id.toString(), _dayIndex));

                    break;
                  case SelectChoice.emailAttendees:
                    new BackendManager().openEmail(_evt.getRoster(
                        new BackendManager().data.people, _dayIndex));
                    return;
                  case SelectChoice.emailLeaders:
                    new BackendManager().openEmail(_evt.leaders
                        .map((int id) => new BackendManager()
                            .data
                            .people
                            .firstWhere((CommServPerson p) => p.id == id))
                        .toList());
                    return;
                }
                setState(() {
                  new BackendManager().updateDataUpdatesPPL(updatedRoster);
                });
                new BackendManager().onRefresh();
              },
              itemBuilder: (BuildContext context) =>
                  (_superUser
                      ? <PopupMenuEntry<SelectChoice>>[
                          const PopupMenuItem<SelectChoice>(
                            value: SelectChoice.all,
                            child: const Text('Confirm All'),
                          ),
                          const PopupMenuItem<SelectChoice>(
                            value: SelectChoice.none,
                            child: const Text('Confirm None'),
                          ),
                          const PopupMenuItem<SelectChoice>(
                            value: SelectChoice.invert,
                            child: const Text('Invert'),
                          ),
                          const PopupMenuItem<SelectChoice>(
                            value: SelectChoice.promote,
                            child: const Text('Promote to Leader'),
                          ),
                          const PopupMenuItem<SelectChoice>(
                            value: SelectChoice.demote,
                            child: const Text('Demote from Leader'),
                          ),
                          const PopupMenuItem<SelectChoice>(
                            value: SelectChoice.add,
                            child: const Text('Add User'),
                          ),
                          const PopupMenuItem<SelectChoice>(
                            value: SelectChoice.remove,
                            child: const Text('Remove User'),
                          ),
                          const PopupMenuItem<SelectChoice>(
                            value: SelectChoice.emailAttendees,
                            child: const Text('Contact Attendees'),
                          ),
                        ]
                      : <PopupMenuEntry<SelectChoice>>[]) +
                  <PopupMenuEntry<SelectChoice>>[
                    const PopupMenuItem<SelectChoice>(
                      value: SelectChoice.emailLeaders,
                      child: const Text('Contact Leaders'),
                    ),
                  ])
        ]),
        body: _buildRosterView());
  }

  Widget _buildRosterView() {
    List<CommServPerson> roster =
        _evt.getRoster(new BackendManager().data.people, _dayIndex);
    roster.sort((CommServPerson p1, CommServPerson p2) {
      if (_evt.leaders.contains(p1.id) && !_evt.leaders.contains(p2.id)) {
        return -1;
      } else if (!_evt.leaders.contains(p1.id) &&
          _evt.leaders.contains(p2.id)) {
        return 1;
      } else {
        return p1.lastName.compareTo(p2.lastName);
      }
    });
    return new RefreshIndicator(
        child: new ListView.builder(
          itemBuilder: (BuildContext context, int index) {
            return _superUser
                ? new CheckboxListTile(
                    value: roster[index]
                        .isConfirmed(_evt.id.toString(), _dayIndex),
                    onChanged: (v) {
                      CommServPerson temp = roster[index].clone();
                      setState(() {
                        v
                            ? temp.confirm(_evt.id.toString(), _dayIndex)
                            : temp.deConfirm(_evt.id.toString(), _dayIndex);

                        new BackendManager().updateDataUpdatesP(temp);
                      });
                      new BackendManager().onRefresh();
                    },
                    title: new Text(
                      roster[index].title,
                      style: new TextStyle(
                          fontWeight: _evt.leaders.contains(roster[index].id)
                              ? FontWeight.bold
                              : FontWeight.normal),
                    ),
                  )
                : new ListTile(
                    title: new Text(
                      roster[index].title,
                      style: new TextStyle(
                          fontWeight: _evt.leaders.contains(roster[index].id)
                              ? FontWeight.bold
                              : FontWeight.normal),
                    ),
                  );
          },
          padding: new EdgeInsets.only(
              left: 16.0, right: 16.0, top: 8.0, bottom: 8.0),
          itemCount: roster.length,
        ),
        key: _refreshIndicatorKey,
        onRefresh: new BackendManager().onRefresh);
  }
}
