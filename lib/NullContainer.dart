import 'package:flutter/material.dart';

class NullContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(height: 0.0, width: 0.0,);
  }
}