// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CommServEventDay.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CommServEventDay _$CommServEventDayFromJson(Map<String, dynamic> json) {
  return new CommServEventDay(
      startTimeMS: json['startTimeMS'] as int,
      endTimeMS: json['endTimeMS'] as int,
      deleted: json['deleted'] as bool);
}

abstract class _$CommServEventDaySerializerMixin {
  int get startTimeMS;
  int get endTimeMS;
  bool get deleted;
  Map<String, dynamic> toJson() => <String, dynamic>{
        'startTimeMS': startTimeMS,
        'endTimeMS': endTimeMS,
        'deleted': deleted
      };
}
