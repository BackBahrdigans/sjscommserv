import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:numberpicker/numberpicker.dart';

import 'BackendManager.dart';
import 'CommServPerson.dart';

class EditProfileView extends StatefulWidget {
  final CommServPerson _person;
  @override
  createState() => new EditProfileViewState(_person.clone());
  EditProfileView(this._person, {Key key}) : super(key: key);
}

class EditProfileViewState extends State<EditProfileView> {
  CommServPerson _person;

  static final DateFormat _gradYearFormatter = new DateFormat("y");
  static final DateFormat _birthdayFormatter = new DateFormat("EEEE MMMM d, y");

  TextEditingController _nameController;
  TextEditingController _studentIDController;
  TextEditingController _phoneController;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  EditProfileViewState(this._person) {
    _nameController = new TextEditingController(text: _person.name);
    _studentIDController =
        new TextEditingController(text: _person.studentID ?? "");
    _phoneController = new TextEditingController(text: _person.phone ?? "");
  }

  void _save() async {
    _person = _person.clone(
      Nname: _nameController.text,
      NstudentID: _studentIDController.text,
      Nphone: _phoneController.text,
    );
    new BackendManager().updateDataUpdatesP(_person);

    Navigator.pop(context, true);
  }

  @override
  Widget build(BuildContext context) {
    new BackendManager().setContext(_scaffoldKey, null, null);
    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        title: new Text("Edit Profile"),
        actions: <Widget>[
          new IconButton(icon: new Icon(Icons.save), onPressed: _save),
        ],
      ),
      body: _buildEditProfileView(),
    );
  }

  void showGradYearPicker() {
    showDialog<int>(
        context: context,
        builder: (BuildContext context) {
          return new NumberPickerDialog.integer(
            minValue: 0,
            maxValue: 2999,
            title: new Text("Graduation Year"),
            initialIntegerValue: new DateTime.now().year,
          );
        }).then((int value) {
      if (value != null) {
        setState(() {
          _person = _person.clone(
              NgradYear: new DateTime(value));
        });
      }
    });
  }

  Widget _buildEditProfileView() {
    return new ListView(
      padding:
          new EdgeInsets.only(left: 16.0, right: 16.0, top: 8.0, bottom: 8.0),
      children: <Widget>[
        new TextField(
          maxLines: 1,
          keyboardType: TextInputType.text,
          controller: _nameController,
          decoration: new InputDecoration(hintText: "Name"),
        ),
        new TextField(
          maxLines: null,
          keyboardType: TextInputType.phone,
          controller: _studentIDController,
          decoration: new InputDecoration(hintText: "Student ID"),
        ),
        new TextField(
          maxLines: null,
          keyboardType: TextInputType.phone,
          controller: _phoneController,
          decoration: new InputDecoration(hintText: "Phone Number"),
        ),
        new ListTile(
          leading: const Icon(Icons.cake),
          title: new Text(_person.birthday == null
              ? "Unset"
              : "${_birthdayFormatter.format(_person.birthday)}"),
          subtitle: new Text("Birthday"),
          onTap: () async {
            final n = await showDatePicker(
              context: context,
              initialDate: _person.birthday ?? new DateTime.now(),
              firstDate:
                  new DateTime.fromMillisecondsSinceEpoch(-99999999999999),
              lastDate: new DateTime.fromMillisecondsSinceEpoch(99999999999999),
            );
            if (n == null) return;
            setState(() => _person = _person.clone(
                  Nbirthday: n,
                ));
          },
        ),
        new ListTile(
          leading: const Icon(Icons.event),
          title: new Text(_person.gradYear == null
              ? "Unset"
              : "${_gradYearFormatter.format(_person.gradYear)}"),
          subtitle: new Text("Grad Year"),
          onTap: showGradYearPicker,
        ),
      ],
    );
  }
}
