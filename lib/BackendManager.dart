import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:url_launcher/url_launcher.dart';

import 'CommServEvent.dart';
import 'CommServPerson.dart';
import 'Data.dart';
import 'Endpoint.dart';
import 'LifecycleEventHander.dart';

class BackendManager {
  static const String CACHE_FILE = "dataCache.json";
  static const String SERVER_ADDRESS_LIST_URL =
      "https://backbahrdigans.gitlab.io/plain-html/servers.json"; // TODO:temp
  static const String APP_NAME = "SJSCommServ";
  static const String APP_VERSION = "1.0.0";
  static const String MAP_KEY = "AIzaSyC_7ccButAvJr8_5Fh5AAUXsiw-gKthihI";

  // singleton creator
  static final BackendManager _bm = new BackendManager._internal();
  factory BackendManager() => _bm;

  String _address;

  BackendManager._internal() {
    WidgetsBinding.instance
        .addObserver(new LifecycleEventHandler(resumeCallBack: createRefresh));
    _userID = -1;
    _isRefreshing = false;
    _latestFailedUpdates = new Data.empty();
  }

  Data _verifiedData;
  Data _dataUpdates = new Data.empty();
  Data _latestFailedUpdates = new Data.empty();
  Data get latestFailedUpdates => _latestFailedUpdates;
  int _userID = -1;
  int _updateID;
  int leaderTimeMultiplier = 0;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final _googleSignIn = new GoogleSignIn(hostedDomain: 'sjs.org');

  String _token;

  int get userID => _userID;
  Data get data => _verifiedData?.resultOfApplying(_dataUpdates);

  bool _isCacheLoaded = false;
  GlobalKey<ScaffoldState> _scaffoldKey;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey;
  Function _setStateFunc;

  bool _isRefreshing;
  bool get isRefreshing => _isRefreshing;

  Future<String> get _localPath async =>
      (await getApplicationDocumentsDirectory()).path;

  Future<File> get _localFile async =>
      new File("${await _localPath}/$CACHE_FILE");

  void updateDataUpdatesE(CommServEvent e) {
    _dataUpdates.replaceWith(new Data.event(e));
  }

  void updateDataUpdatesP(CommServPerson p) {
    _dataUpdates.replaceWith(new Data.person(p));
  }

  void updateDataUpdatesPPL(List<CommServPerson> p) {
    _dataUpdates.replaceWith(new Data.ppl(p));
  }

  //void updateDataUpdatesEP(CommServEvent e, CommServPerson p) {
  //_dataUpdates.replaceWith(new Data(events: [e], people: [p]));
  //}

  // every activity needs to call this asap
  void setContext(GlobalKey<ScaffoldState> scaffoldKey,
      GlobalKey<RefreshIndicatorState> refreshKey, Function setStateFunc) {
    _scaffoldKey = scaffoldKey;
    _refreshIndicatorKey = refreshKey;
    _setStateFunc = setStateFunc ?? (Function f) {};
  }

  Future<bool> createRefresh() async {
    _refreshIndicatorKey?.currentState?.show() ?? onRefresh();
    return true;
  }

  Future<Null> onRefresh() async {
    await _refresh();
  }

  Future<Null> _writeCache() async {
    (await _localFile).writeAsString(_toJsonString());
  }

  Future<Null> loadCache() async {
    if (_isCacheLoaded) return;
    Data _newD;
    try {
      final map = jsonDecode(await (await _localFile).readAsString());
      _newD = new Data.fromJson(map['data']);
      _userID = map['user_id'];
      _updateID = map['update_id'];
      leaderTimeMultiplier = map['leaderTimeMultiplier'];
      if ([_newD, _userID, _updateID].contains(null)) throw new Exception();
    } catch (e) {
      //print(await (await _localFile).readAsString());
      print("Error loading cache: $e");
      _newD = new Data(
          events: new List<CommServEvent>(),
          people: new List<CommServPerson>());
      _userID = -69;
      _updateID = -69;
      leaderTimeMultiplier = 2;
    }
    _setStateFunc(() => _verifiedData = _newD);
    createRefresh();
    //_data = jsonDecode(source);
    _isCacheLoaded = true;
  }

  String _toJsonString() =>
      //const JsonEncoder.withIndent(' ').convert(obj);
      jsonEncode({
        'update_id': _updateID,
        'user_id': userID,
        'data': _verifiedData
      }); // does this recurse properly?

  void _showSnackbar(String text) {
    _scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }

  List<Endpoint> _requests = new List<Endpoint>();
  bool _isRunning = false;

  Future<dynamic> _queueRequest(Endpoint e) {
    e.completer = new Completer();
    _requests.add(e);
    if (!_isRunning) {
      _isRunning = true;
      _startCalling();
    }
    return e.completer.future;
  }

  void _startCalling() async {
    _isRunning = true;
    final List<Endpoint> requestsTodo = new List.from(_requests);
    for (int i = 0; i < requestsTodo.length; i++) {
      requestsTodo[i].completer.complete(
          await _callEndpoint(requestsTodo[i].endpoint, requestsTodo[i].data));
    }
    _requests.removeWhere((Endpoint e) {
      for (int i = 0; i < requestsTodo.length; i++) {
        if (e == requestsTodo[i]) {
          return true;
        }
      }
      return false;
    });
    _isRunning = false;
    if (_requests.length != 0) {
      _startCalling();
    }
  }

  Future<Null> _getAddress() async {
    String respU = (await http.get(SERVER_ADDRESS_LIST_URL)).body;
    dynamic s = json.decode(respU);
    _address = s[0]['ip'];
  }

  Future<dynamic> _callEndpoint(String endpoint, String sData) async {
    try {
      while (_address == null) {
        await _getAddress();
      }
      final url = new Uri.http("$_address", "v$APP_VERSION$endpoint");
      print("*********** Calling $url with $sData");
      while (_token == null) {
        await _login();
      }

      http.Response respU = await http.post(
        url,
        body: sData,
        headers: {
          HttpHeaders.AUTHORIZATION:
              "Basic ${base64Encode(const Latin1Codec().encode(':$_token'))}",
          HttpHeaders.CONTENT_TYPE: "application/json",
        },
      );

      print("got response ${respU.body}");

      if (respU.statusCode == 401) {
        print("Error 401. token = $_token");
        await logout();
        return _callEndpoint(endpoint, sData);
      }

      if (respU.statusCode != 200) {
        print("sc ${respU.statusCode}");
        _showSnackbar("Server error ${respU.statusCode}.");
        return null;
      }

      var resp = jsonDecode(respU.body);
      return resp;
    } catch (s) {
      print("ERROR SOMEWHERE $s");
      if (s is SocketException) {
        _showSnackbar("No internet connection.");
      } else {
        print("Internal server error.");
        return _callEndpoint(endpoint, sData); //TODO: such a bad fix lmao
      }
      return null;
    }
  }

  Future<bool> fullRefresh() async {
    return _refresh(full: true);
  }

  Future<bool> _refresh({bool full = false}) async {
    if (_isRefreshing) return false;
    if (!_isCacheLoaded) await loadCache();
    _isRefreshing = true;
    Data tempDataUpdates = _dataUpdates.clone();
    if (full) {
      _isRefreshing = false;
      _updateID = -42;
      _userID = -48;
      leaderTimeMultiplier = 0;
      tempDataUpdates = new Data.empty();
      _verifiedData = new Data.empty();
      _dataUpdates = new Data.empty();
    }
    print("starting");
    final resp = await _queueRequest(new Endpoint(
        "/update",
        jsonEncode(
            {'update_id': _updateID, 'data': tempDataUpdates.toJson()})));
    print("got update $resp");
    if (resp == null) {
      _isRefreshing = false;
      return false;
    }
    //try {
    Data verifiedUpdates = new Data.fromJson(resp['serverData']);
    Data changesUpdates = new Data.fromJson(resp['clientData']);
    Data failedUpdates = new Data.fromJson(resp['failedData']);

    if ([_userID, _updateID, verifiedUpdates, changesUpdates].contains(null))
      throw new Exception("were so dumb");
    _setStateFunc(() {
      if (full) {
        _verifiedData = null;
        _dataUpdates = new Data.empty();
      }
      if (_verifiedData != null) {
        _verifiedData.replaceWith(verifiedUpdates..replaceWith(changesUpdates));
      } else {
        _verifiedData = verifiedUpdates..replaceWith(changesUpdates);
      }
      _dataUpdates.removeEqual(
          tempDataUpdates); // TODO: add scaffold context for porfile
      _dataUpdates.removeEqual(changesUpdates);
      _dataUpdates.removeEqual(failedUpdates);

      _userID = resp['user_id'];
      _updateID = resp['update_id'];
      leaderTimeMultiplier = resp['leaderTimeMultiplier'];
    });
    if (failedUpdates.events.length != 0) {
      print(" events are ${failedUpdates.events}");
      _showSnackbar("Failed to " +
          (failedUpdates.events[0].id == -1 ? "add" : "update") +
          " event " +
          failedUpdates.events[0].name);
      _latestFailedUpdates = failedUpdates;
    } else {
      _latestFailedUpdates = new Data.empty();
    }

    print("did it!");
    _writeCache();
    /*} catch (e) {
      print(e);
      _showSnackbar("Internal server error.");
      _isRefreshing = false;
      return false;
    }*/
    _isRefreshing = false; // TODO: delete didnt work!
    return true; // TODO: fix bolding
  } // TODO: event locking is broken

//TODO: round to nearest 15 minutes (7 -> 0, 8 -> 15)
  //TODO: double ishan's hours when hes a leader on the client, make it say *2
  //TODO: add column for user's name in get one person's hours spreadsheet button
  //TODO: add triple dots button -> invert on days
  //TODO: make it so officers can see other people's profiles
  //TODO: app name: volunteer manager
  //TODO: talk to jeff and larry henderson
  //TODO: 77079 zip code
  //TODO: make video for teaching how to use app from different perspectives
  Future<dynamic> getLock(int id) async {
    final resp = await _queueRequest(
        new Endpoint("/lock/get", jsonEncode({'event_id': id})));
    if (resp == null) {
      return null;
    }
    return resp;
  }

  Future<bool> releaseLock(int id) async {
    final resp = await _queueRequest(
        new Endpoint("/lock/release", jsonEncode({'event_id': id})));
    if (resp == null) {
      return false;
    }
    return resp['result'];
  }

  void _showExportSnackbar() {
    _showSnackbar("A spreadsheet has been emailed to you.");
  }

  Future<Null> export({int id}) async {
    int realID = id ?? _userID;
    await _queueRequest(
        new Endpoint("/getHours", jsonEncode({'user_id': realID})));
    _showExportSnackbar();
  }

  Future<Null> exportAll() async {
    await _queueRequest(new Endpoint("/getHoursSuper", jsonEncode({})));
    _showExportSnackbar();
  }

  DateTime _normal(DateTime input) {
    return new DateTime(input.year, input.month, input.day);
  }

  Map<int, Map<CommServEvent, List<int>>> getEventsOnMonth(DateTime dt) {
    var events = data.events;

    var ret = new Map<int, Map<CommServEvent, List<int>>>();
    for (int i = -7; i < 42; i++) {
      ret[i] = new Map<CommServEvent, List<int>>();
    }
    if (data.user == null) return ret;
    for (int i = 0; i < events.length; i++) {
      for (int j = 0; j < events[i].eventDays.length; j++) {
        if (events[i].eventDays[j].deleted || !data.user.canSee(events[i]))
          continue;
        final d = _normal(events[i].eventDays[j].startTime)
            .difference(_normal(dt))
            .inDays;
        if (d < 42 && d > -8) {
          if (!ret[d].containsKey(events[i])) {
            ret[d][events[i]] = new List<int>();
          }
          ret[d][events[i]].add(j);
        }
      }
    }

    return ret;
  }

  Map<CommServEvent, List<int>> getEventsOnDay(DateTime dt) {
    var events = data.events;

    var ret = new Map<CommServEvent, List<int>>();
    for (int i = 0; i < events.length; i++) {
      for (int j = 0; j < events[i].eventDays.length; j++) {
        if (!events[i].eventDays[j].deleted &&
            data.user.canSee(events[i]) &&
            events[i].eventDays[j].startTime.year == dt.year &&
            events[i].eventDays[j].startTime.month == dt.month &&
            events[i].eventDays[j].startTime.day == dt.day) {
          if (!ret.containsKey(events[i])) {
            ret[events[i]] = new List<int>();
          }
          ret[events[i]].add(j);
        }
      }
    }

    return ret;
  }

  Future<Null> _login() async {
    try {
      print("Logging in!");
      if (_token != null) return;
      print("Awaiting");
      GoogleSignInAccount user = await _googleSignIn.currentUser;
      if (user == null) {
        print("Awaiting 2");
        user = await _googleSignIn.signInSilently();
      }
      while (user == null) {
        print("Awaiting 3");
        user = await _googleSignIn.signIn();
      }
      print("Got user.");

      var cUser = await _auth.currentUser();
      if (cUser == null) {
        GoogleSignInAuthentication credentials = await user.authentication;
        print("Got user auth.");
        cUser = await _auth.signInWithGoogle(
          idToken: credentials.idToken,
          accessToken: credentials.accessToken,
        );
      }
      print("Got firebase user.");
      print("Signed in ${user.displayName}.");
      if (_token == null) {
        _token = await cUser.getIdToken();
        _profileURI = user.photoUrl;
      }
    } catch (e) {
      _token = null;
      return;
    }
  }

  Future<Null> logout() async {
    print("Logging out");
    _token = null;
    await _googleSignIn.signOut();
    await _auth.signOut();
  }

  String _profileURI;
  String get profileURI => _profileURI;

  void openEmail(List<CommServPerson> people) async {
    final String url =
        "mailto:${people.map((CommServPerson p) => p.email).toList().join(",")}";
    if (await canLaunch(url)) {
      launch(url);
    }
  }
}
