import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'BackendManager.dart';
import 'CommServPerson.dart';
import 'EditProfileView.dart';
import 'HelpView.dart';
import 'LogView.dart';
import 'NullContainer.dart';
import 'SelectUserView.dart';
import 'SettingsView.dart';

enum Selection {
  addOfficer,
  removeOfficer,
  addAdmin,
  removeAdmin,
  exportSomeone,
  exportYou,
  exportAll,
}

class ProfileScreen extends StatefulWidget {
  @override
  createState() => new ProfileScreenState();
}

class ProfileScreenState extends State<ProfileScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    new BackendManager().setContext(_scaffoldKey, null, setState);
    return new Scaffold(
        key: _scaffoldKey,
        appBar: new AppBar(
          title: new Text("Profile"),
          leading: new IconButton(
              icon: new Icon(Icons.arrow_back),
              onPressed: () => Navigator.pop(context)),
          actions: <Widget>[
            new IconButton(
                icon: new Icon(Icons.help),
                onPressed: () {
                  Navigator.push(
                      this.context,
                      new MaterialPageRoute(
                          builder: (context) => new HelpView()));
                }),
            new IconButton(
                icon: new Icon(Icons.settings),
                onPressed: () {
                  Navigator.push(
                      this.context,
                      new MaterialPageRoute(
                          builder: (context) => new SettingsView()));
                }),
            new IconButton(
                icon: new Icon(Icons.edit),
                onPressed: () async {
                  final res = await Navigator.push(
                      this.context,
                      new MaterialPageRoute(
                          builder: (context) => new EditProfileView(
                              new BackendManager().data.user)));
                  if (res != true) return;
                  setState(() {});
                  await new BackendManager().onRefresh();
                  _scaffoldKey.currentState.showSnackBar(
                      new SnackBar(content: new Text("Updated user.")));
                }),
            PopupMenuButton<Selection>(
              onSelected: (Selection result) async {
                List<CommServPerson> updatedRoster = new List<CommServPerson>();
                switch (result) {
                  case Selection.addOfficer:
                    final List<CommServPerson> users = new BackendManager()
                        .data
                        .people
                        .where((CommServPerson p) => !p.isPersonOfficer())
                        .toList()
                          ..sort(
                              (p1, p2) => p1.lastName.compareTo(p2.lastName));
                    final int successIndex = await Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new SelectUserView(
                                "promote", // TODO: why is snackbar not showing on spreadsheet email
                                true,
                                users,
                                "user",
                                "users",
                                true)));
                    if (successIndex == null) return;

                    new BackendManager().updateDataUpdatesP(
                        users[successIndex].clone(NisOfficer: 1));

                    break;
                  case Selection.removeOfficer:
                    final List<CommServPerson> officers = new BackendManager()
                        .data
                        .people
                        .where((CommServPerson p) => p.isOfficer == 1)
                        .toList()
                          ..sort(
                              (p1, p2) => p1.lastName.compareTo(p2.lastName));
                    final int successIndex = await Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new SelectUserView("demote",
                                true, officers, "officer", "officers", true)));
                    if (successIndex == null) return;

                    new BackendManager().updateDataUpdatesP(
                        officers[successIndex].clone(NisOfficer: 0));

                    break;
                  case Selection.addAdmin:
                    final List<CommServPerson> officers = new BackendManager()
                        .data
                        .people
                        .where((CommServPerson p) => p.isOfficer == 1)
                        .toList()
                          ..sort(
                              (p1, p2) => p1.lastName.compareTo(p2.lastName));
                    final int successIndex = await Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new SelectUserView("promote",
                                true, officers, "officer", "officers", true)));
                    if (successIndex == null) return;

                    new BackendManager().updateDataUpdatesP(
                        officers[successIndex].clone(NisOfficer: 2));

                    break;
                  case Selection.removeAdmin:
                    final List<CommServPerson> admins = new BackendManager()
                        .data
                        .people
                        .where((CommServPerson p) => p.isSupremeLeader())
                        .toList()
                          ..sort(
                              (p1, p2) => p1.lastName.compareTo(p2.lastName));
                    final int successIndex = await Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new SelectUserView(
                                "demote",
                                true,
                                admins,
                                "Administrator",
                                "Administrators",
                                true)));
                    if (successIndex == null) return;

                    new BackendManager().updateDataUpdatesP(
                        admins[successIndex].clone(NisOfficer: 1));

                    break;
                  case Selection.exportAll:
                    new BackendManager().exportAll();
                    return;
                  case Selection.exportSomeone:
                    final int successIndex = await Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new SelectUserView(
                                "get hours", // TODO: make it support two words
                                true,
                                new BackendManager().data.people,
                                "User",
                                "Users",
                                true)));
                    if (successIndex == null) return;
                    new BackendManager().export(
                        id: new BackendManager().data.people[successIndex].id);
                    return;
                  case Selection.exportYou:
                    new BackendManager().export();
                    return;
                }
                setState(() {
                  new BackendManager().updateDataUpdatesPPL(updatedRoster);
                });
                new BackendManager().onRefresh();
              },
              itemBuilder: (BuildContext context) =>
                  (new BackendManager().data.user.isSupremeLeader()
                      ? <PopupMenuEntry<Selection>>[
                          const PopupMenuItem<Selection>(
                            value: Selection.addOfficer,
                            child: const Text('Promote User to Officer'),
                          ),
                          const PopupMenuItem<Selection>(
                            value: Selection.removeOfficer,
                            child: const Text('Demote Officer to User'),
                          ),
                          const PopupMenuItem<Selection>(
                            value: Selection.addAdmin,
                            child:
                                const Text('Promote Officer to Administrator'),
                          ),
                          new BackendManager()
                                      .data
                                      .people
                                      .where((CommServPerson p) =>
                                          p.isSupremeLeader())
                                      .toList()
                                      .length >
                                  1
                              ? const PopupMenuItem<Selection>(
                                  value: Selection.removeAdmin,
                                  child: const Text(
                                      'Demote Adminsitrator to Officer'),
                                )
                              : null,
                          const PopupMenuItem<Selection>(
                            value: Selection.exportAll,
                            child: const Text("Export All Hours"),
                          ),
                          const PopupMenuItem<Selection>(
                            value: Selection.exportSomeone,
                            child: const Text("Export a User's Hours"),
                          ),
                        ]
                      : <PopupMenuEntry<Selection>>[]) +
                  <PopupMenuEntry<Selection>>[
                    const PopupMenuItem<Selection>(
                      value: Selection.exportYou,
                      child: const Text('Export Your Hours'),
                    ),
                  ],
            )
          ],
        ),
        body: new Builder(
            builder: (BuildContext context) => _buildProfile(context)));
  }

  ListTile _getUserInfoListTile() {
    var user = new BackendManager().data.user;
    var str = "";
    str += (user.birthday != null
        ? "Born " + new DateFormat("MMMM d, yyyy").format(user.birthday) + '\n'
        : "");
    str += ((user.phone != null && user.phone.isNotEmpty)
        ? "Phone: " + user.phone + '\n'
        : "");
    str += ((user.studentID != null && user.studentID.isNotEmpty)
        ? "Student ID: " + user.studentID + '\n'
        : "");
    str += ((user.gradYear != null)
        ? "Class of " + user.gradYear.year.toString() + '\n'
        : "");
    str = str.trim();
    if (str.length == 0) {
      return new ListTile(
          leading: const Icon(Icons.info),
          title: new Text("Update your info with the edit button above"));
    } else {
      return new ListTile(
        leading: const Icon(Icons.info),
        subtitle: new Text(str),
      );
    }
  }

  Widget _buildProfile(BuildContext context) {
    return new ListView(children: [
      new Padding(
          padding: new EdgeInsets.only(
              top: 16.0, bottom: 8.0, left: 16.0, right: 16.0),
          child: new Row(
            children: [
              new Padding(
                padding: new EdgeInsets.only(right: 16.0),
                child: new CircleAvatar(
                  backgroundImage:
                      new NetworkImage(new BackendManager().profileURI),
                ),
              ),
              new Text(
                new BackendManager().data.user.name,
                style: new TextStyle(
                  fontSize: 28.0,
                ),
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.center,
          )),
      _getUserInfoListTile(),
      new Divider(),
      new ListTile(
        leading: const Icon(Icons.timer),
        title: new Text(
            (new BackendManager().data.userMinutes / 60).toStringAsFixed(1)),
        subtitle: new Text("Cumulative service hours"),
      ),
      new Divider(),
      new ListTile(
        leading: const Icon(Icons.list),
        title: new Text("Upcoming commitments"),
      ),
      new Padding(
          padding: new EdgeInsets.only(left: 16.0, right: 16.0, bottom: 8.0),
          child: new BackendManager().data.futureUserEvents.length > 0
              ? new LogView(3, EventType.future)
              : new Text("Nothing upcoming")),
      new BackendManager().data.futureUserEvents.length > 3
          ? new FlatButton(
              onPressed: () {
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) =>
                            new LogViewScreen(EventType.future)));
              },
              child: new Text("View All Upcoming"),
            )
          : new NullContainer(),
      new Divider(),
      new ListTile(
        leading: const Icon(Icons.list),
        title: new Text("Completed commitments"),
      ),
      new Padding(
          padding: new EdgeInsets.only(left: 16.0, right: 16.0, bottom: 8.0),
          child: new BackendManager().data.pastUserEvents.length > 0
              ? new LogView(3, EventType.past)
              : new Text("Nothing completed")),
      new BackendManager().data.pastUserEvents.length > 3
          ? new FlatButton(
              onPressed: () {
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) =>
                            new LogViewScreen(EventType.past)));
              },
              child: new Text("View All Completed"),
            )
          : new NullContainer(),
      /*new FlatButton(
            onPressed: () {
              Scaffold.of(context).showSnackBar(
                  new SnackBar(content: new Text("Coming soon!"))
              );
            },
            child: new Text("OPEN LOG"),
        ),
        new FlatButton(
            onPressed: () {
              setState(() {
                var event = new CommServEvent(
                  3, new DateTime.now().millisecondsSinceEpoch,
                  new DateTime.now().millisecondsSinceEpoch+7200000,
                  "Write this app", "meems", null,
                  null, null, [], [], true, false,
                );
                addEventToUpcoming(event);
              });
            },
            child: new Text("ADD EVENT"),
        )*/
    ]);
  }
}
