// TO BUILD JSON:
// cd sjscommserv
// flutter packages pub run build_runner build --delete-conflicting-outputs

import 'package:json_annotation/json_annotation.dart';

part "Location.g.dart";

@JsonSerializable(nullable: false)
class Location extends Object with _$LocationSerializerMixin {
  final double latitude;
  final double longitude;
  Location({this.latitude, this.longitude});
  factory Location.fromJson(Map<String, dynamic> json) =>
      _$LocationFromJson(json);

  Location clone({double Nlatitude, double Nlongitude}) {
    return new Location(
        latitude: Nlatitude ?? latitude, longitude: Nlongitude ?? longitude);
  }
}
