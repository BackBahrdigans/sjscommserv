import 'package:json_annotation/json_annotation.dart';

import 'CommServEvent.dart';
import 'BackendManager.dart';

part "CommServPerson.g.dart";

@JsonSerializable(nullable: true)
class CommServPerson extends Object with _$CommServPersonSerializerMixin {
  final String name;
  final String email;
  final int id;
  final int isOfficer;

  final Map<String, List<int>> eventsSignedUpFor;
  final Map<String, List<int>> eventsConfirmedFor;

  DateTime gradYear;
  String studentID;
  DateTime birthday;
  String phone;

  CommServPerson({
    this.id,
    this.name,
    this.email,
    this.isOfficer,
    this.eventsSignedUpFor,
    this.eventsConfirmedFor,
    this.gradYear,
    this.studentID,
    this.birthday,
    this.phone,
  });
  factory CommServPerson.fromJson(Map<String, dynamic> json) =>
      _$CommServPersonFromJson(json);

  String get title => "$name ($email)";
  String get lastName => name.split(" ").removeLast();

  bool isGoing(String id, int index) {
    if (eventsSignedUpFor.containsKey(id)) {
      if (eventsSignedUpFor[id].contains(index)) {
        return true;
      }
    }
    return false;
  }

  void register(String id, int index) {
    if (!eventsSignedUpFor.containsKey(id)) {
      eventsSignedUpFor[id] = new List<int>();
    }
    eventsSignedUpFor[id].add(index);
  }

  void deRegister(String id, int index) {
    deConfirm(id, index);
    eventsSignedUpFor[id]?.remove(index);
    //if (eventsSignedUpFor[id]?.length == 0) {
    //eventsSignedUpFor.remove(id);
    //}
  }

  bool isConfirmed(String id, int index) {
    if (eventsConfirmedFor.containsKey(id)) {
      if (eventsConfirmedFor[id].contains(index)) {
        return true;
      }
    }
    return false;
  }

  void confirm(String id, int index) {
    if (!eventsConfirmedFor.containsKey(id)) {
      eventsConfirmedFor[id] = new List<int>();
    }
    eventsConfirmedFor[id].add(index);
  }

  void deConfirm(String id, int index) {
    eventsConfirmedFor[id]?.remove(index);
    /*if (eventsConfirmedFor[id]?.length == 0) {
      eventsConfirmedFor.remove(id);
    }*/
  }

  int getCumulativePastMinutes(List<CommServEvent> allEvents) {
    int sum = 0;
    DateTime now = new DateTime.now();
    for (var pastEvent in allEvents) {
      for (int i = 0; i < pastEvent.eventDays.length; i++) {
        if (now.isAfter(pastEvent.eventDays[i].endTime) &&
            isConfirmed(pastEvent.id.toString(), i))
          if (pastEvent.leaders.contains(new BackendManager().data.user.id)) {
            sum += pastEvent.eventDays[i].effectiveMinutes * new BackendManager().leaderTimeMultiplier;
          } else {
            sum += pastEvent.eventDays[i].effectiveMinutes;
          }
      }
    }
    return sum;
  }

  bool canSee(CommServEvent evt) {
    return !evt.isHidden || isPersonOfficer();
  }

  bool isSuperUserOfEvent(CommServEvent e) {
    return isPersonOfficer() || e.leaders.contains(id);
  }

  bool isPersonOfficer() {
    return isOfficer > 0;
  }

  bool isSupremeLeader() {
    return isOfficer > 1;
  }

  CommServPerson clone({
    int Nid,
    String Nname,
    String Nemail,
    int NisOfficer,
    Map<String, List<int>> NeventsSignedUpFor,
    Map<String, List<int>> NeventsConfirmedFor,
    DateTime NgradYear,
    String NstudentID,
    DateTime Nbirthday,
    String Nphone,
  }) {
    return new CommServPerson(
      id: Nid ?? id,
      name: Nname ?? name,
      email: Nemail ?? email,
      isOfficer: NisOfficer ?? isOfficer,
      eventsSignedUpFor: NeventsSignedUpFor ??
          new Map.from(eventsSignedUpFor.map((String str, List<int> val) {
            return new MapEntry(str, new List<int>.from(val));
          })),
      eventsConfirmedFor: NeventsConfirmedFor ??
          new Map.from(eventsConfirmedFor.map((String str, List<int> val) {
            return new MapEntry(str, new List<int>.from(val));
          })),
      gradYear: NgradYear ??
          (gradYear == null
              ? null
              : new DateTime.fromMillisecondsSinceEpoch(
                  gradYear.millisecondsSinceEpoch)),
      studentID: NstudentID ?? studentID,
      birthday: Nbirthday ??
          (birthday == null
              ? null
              : new DateTime.fromMillisecondsSinceEpoch(
                  birthday.millisecondsSinceEpoch)),
      phone: Nphone ?? phone,
    );
  }
}
