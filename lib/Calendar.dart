import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'BackendManager.dart';
import 'CommServEvent.dart';
import 'CommServEventDay.dart';
import 'DayView.dart';
import 'EditEventView.dart';
import 'EditSummerEventView.dart';
import 'Location.dart';
import 'Profile.dart';
import 'UpdatingCalendarTodayWidget.dart';

class Calendar extends StatefulWidget {
  @override
  createState() => new CalendarState();
  Calendar({Key key}) : super(key: key) {
    //new BackendManager().fullRefresh(1234);
  }
}

class CalendarState extends State<Calendar> {
  static final DateFormat _headerFormatter = new DateFormat("MMMM y");
  Future<bool> _isLoaded;
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  CalendarState() : super() {
    _isLoaded = new BackendManager().loadCache();
    _pageController = new PageController(initialPage: _initialPage);
  }

  String _header = _headerFormatter.format(new DateTime.now());
  final int _initialPage = getMonthNumFromDT(new DateTime.now());
  PageController _pageController;

  static DateTime getDTFromMonthNum(int monthNum) {
    return new DateTime(1970, monthNum);
  }

  static int getMonthNumFromDT(DateTime dt) {
    return dt.month + (dt.year - 1970) * 12;
  }

  void _addEvt(bool hidden) async {
    CommServEvent e = new CommServEvent(
      id: -1,
      eventDays: new List<CommServEventDay>()
        ..add(new CommServEventDay.dt(new DateTime.now(), new DateTime.now())),
      name: "",
      description: "",
      parkingLocation: new Location(
          latitude: 29.711813,
          longitude: -95.4249836), // TODO: default location
      parkingInfo: "",
      leaders: new List<int>(),
      isHidden: hidden,
      limit: -1,
      files: [],
    );
    bool saved;
    if (!hidden) {
      saved = await Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (context) => new EditEventView(e))) ??
          false;
    } else {
      saved = await Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (context) => new EditSummerEventView(e))) ??
          false;
    }
    if (!saved) return;

    Completer<BuildContext> comp = new Completer();
    Future.wait([
      new BackendManager().onRefresh(),
    ])
      ..then((val) async {
        Navigator.of(await comp.future).pop();
      });

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          comp.complete(context);
          return new AlertDialog(
              content: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                new CircularProgressIndicator(),
              ]));
        });
  }

  @override
  Widget build(BuildContext context) {
    new BackendManager()
        .setContext(_scaffoldKey, _refreshIndicatorKey, setState);
    bool ready = (new BackendManager()?.data?.isProfilePageReady() ==
            true) // this is to remove null oddities
        ? true
        : false;
    return new Scaffold(
        key: _scaffoldKey,
        appBar: new AppBar(
          title: new Text(_header),
          leading: new IconButton(
            icon: new Icon(ready ? Icons.person : Icons.refresh),
            onPressed: ready
                ? _openProfile
                : (new BackendManager().isRefreshing
                    ? () {}
                    : () async {
                        await new BackendManager().fullRefresh();
                      }),
          ),
          actions: (new BackendManager().data?.user?.isPersonOfficer() == true
                  ? <Widget>[
                      new IconButton(
                        icon: new Icon(Icons.wb_sunny),
                        onPressed: () async {
                          await _addEvt(true);
                        },
                      ),
                      new IconButton(
                          icon: new Icon(Icons.add),
                          onPressed: () async {
                            await _addEvt(false);
                          })
                    ]
                  : <Widget>[]) +
              <Widget>[
                new UpdatingCalendarTodayWidget(
                  setState,
                  _pageController,
                  _initialPage,
                ),
              ],
        ),
        body: _buildCalendarPageView());
  }

  Widget _buildCalendarPageView() {
    return new Container(child: new LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
      return new FutureBuilder<bool>(
          future:
              _isLoaded, // this is basically a cheese way of saying wait till cache is loaded before displaying anything
          builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
              case ConnectionState.waiting:
                return new Center(
                  child: new SizedBox(
                      width: 40.0,
                      height: 40.0,
                      child: new CircularProgressIndicator()),
                );
              default:
                if (snapshot.hasError)
                  return new Text('Error: ${snapshot.error}');
                else
                  return new RefreshIndicator(
                    key: _refreshIndicatorKey,
                    child: new SingleChildScrollView(
                        physics: new AlwaysScrollableScrollPhysics(),
                        child: new Container(
                            height: constraints.maxHeight,
                            child: new PageView.builder(
                                controller: _pageController,
                                itemBuilder: (BuildContext context, int index) {
                                  return new Padding(
                                    padding: new EdgeInsets.only(
                                        left: 16.0, right: 16.0, top: 8.0),
                                    child: _buildCalendar(index, context),
                                  );
                                },
                                onPageChanged: (int monthNum) {
                                  setState(() {
                                    _header = _headerFormatter
                                        .format(getDTFromMonthNum(monthNum));
                                  });
                                }))),
                    onRefresh: new BackendManager().onRefresh,
                  );
            }
          });
    }));
  }

  Widget _buildCalendar(int monthNum, BuildContext context) {
    final weeks = <Widget>[];
    final formatter = new DateFormat.d();
    final formatter2 = new DateFormat.E();
    var style;
    var firstDateTime = new DateTime(
        getDTFromMonthNum(monthNum).year,
        getDTFromMonthNum(monthNum).month,
        1 - getDTFromMonthNum(monthNum).weekday % 7);

    var topRow = <Widget>[];
    for (int i = 0; i < 7; i++) {
      final day = new DateTime.fromMillisecondsSinceEpoch(
          firstDateTime.millisecondsSinceEpoch +
              new Duration(days: i).inMilliseconds);
      topRow.add(new Expanded(
          child: new Text(
        formatter2.format(day),
        style: new TextStyle(fontWeight: FontWeight.bold),
      )));
    }
    weeks.add(new Row(children: topRow));

    final allEvts =
        new BackendManager().getEventsOnMonth(getDTFromMonthNum(monthNum));

    for (var i = 0; i < 6; i++) {
      final days = <Widget>[];
      for (var j = 0; j < 7; j++) {
        final diff = new Duration(days: i * 7 + j);
        final day = firstDateTime.add(diff);
        final text = formatter.format(day);

        if (day.year == new DateTime.now().year &&
            day.month == new DateTime.now().month &&
            day.day == new DateTime.now().day) {
          style = new TextStyle(
              fontWeight: FontWeight.bold,
              decoration: TextDecoration.underline);
        } else {
          style = new TextStyle();
        }

        final Map<CommServEvent, List<int>> evts =
            allEvts[day.difference(getDTFromMonthNum(monthNum)).inDays];
        final user = new BackendManager().data.user;
        List<Widget> evtWidgets = <Widget>[];
        evtWidgets.add(new Text(text, style: style));
        evts.forEach((CommServEvent evt, List<int> dayIndices) {
          for (int k = 0; k < dayIndices.length; k++) {
            evtWidgets.add(new Text(
              evt.name.trim(),
              overflow: TextOverflow.clip,
              maxLines: 1,
              style: new TextStyle(
                  fontWeight: user.isGoing(evt.id.toString(), dayIndices[k])
                      ? FontWeight.bold
                      : FontWeight.normal),
            ));
          }
        });

        days.add(new Expanded(
          child: new InkWell(
              onTap: () {
                Navigator.push(
                    this.context,
                    new MaterialPageRoute(
                        builder: (context) => new DayView(day)));
              },
              child: new ListView(
                children: evtWidgets,
                physics: new NeverScrollableScrollPhysics(),
                //crossAxisAlignment: CrossAxisAlignment.start,
              )),
        ));
        //..add(new Container(width: 1.0, color: Colors.black));
      }
      weeks.add(new Expanded(
          child: new Row(
        children: days,
        crossAxisAlignment: CrossAxisAlignment.stretch,
      )));
      if (i != 5) {
        weeks.add(new Container(
          height: 1.0,
          color: Colors.grey.withOpacity(0.2),
        ));
      }
    }

    return new Column(
        children: weeks, crossAxisAlignment: CrossAxisAlignment.start);
  }

  void _openProfile() {
    Navigator.push(context,
        new MaterialPageRoute(builder: (context) => new ProfileScreen()));
  }
}
