import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:map_view/map_view.dart' as mv;
import 'package:numberpicker/numberpicker.dart';
import 'package:url_launcher/url_launcher.dart';

import 'BackendManager.dart';
import 'CommServEvent.dart';
import 'CommServEventDay.dart';
import 'CommServPerson.dart';
import 'CompositeSubscription.dart';
import 'Location.dart';
import 'NullContainer.dart';

enum Seleccion { all, none, invert }

class EditEventView extends StatefulWidget {
  final CommServEvent _evt;
  @override
  createState() => new EditEventViewState(_evt);
  EditEventView(this._evt, {Key key}) : super(key: key);
}

class EditEventViewState extends State<EditEventView> {
  CommServEvent _evt;
  static final DateFormat _timeFormatter = new DateFormat("jm");
  static final DateFormat _dayFormatter = new DateFormat("EEEE MMMM d, y");
  mv.MapView mapView = new mv.MapView();
  var compositeSubscription = new CompositeSubscription();

  TextEditingController _nameController;
  TextEditingController _descController;
  TextEditingController _parkController;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  mv.Location _tempLoc;

  EditEventViewState(this._evt) {
    _nameController = new TextEditingController(text: _evt.name);
    _descController = new TextEditingController(text: _evt.description);
    _parkController = new TextEditingController(text: _evt.parkingInfo);
    _repeating = _evt.eventDays.length != 1;
    final days = new List<CommServEventDay>();
    for (int i = 0; i < _evt.eventDays.length; i++) {
      final st = _evt.eventDays[i].startTime;
      final et = _evt.eventDays[i].endTime;
      days.add(_evt.eventDays[i].clone(
        NstartTimeMS:
            new DateTime(st.year, st.month, st.day, st.hour, st.minute)
                .millisecondsSinceEpoch,
        NendTimeMS: new DateTime(et.year, et.month, et.day, et.hour, et.minute)
            .millisecondsSinceEpoch,
      ));
    }
    _evt = _evt.clone(NeventDays: days);
  }

  void _save() async {
    _evt = _evt.clone(
      Nname: _nameController.text,
      Ndescription: _descController.text,
      NparkingInfo: _parkController.text,
    );
    new BackendManager().updateDataUpdatesE(_evt);

    final fullPeople = new BackendManager().data.people;
    final people = new List<CommServPerson>();
    for (int j = 0; j < _evt.eventDays.length; j++) {
      if (_evt.eventDays[j].deleted) {
        for (int i = 0; i < fullPeople.length; i++) {
          if (fullPeople[i].eventsSignedUpFor.containsKey(_evt.id.toString())) {
            if (fullPeople[i]
                .eventsSignedUpFor[_evt.id.toString()]
                .contains(j)) {
              people.add(fullPeople[i].clone()
                ..deConfirm(_evt.id.toString(), j)
                ..deRegister(_evt.id.toString(), j));
            }
          }
        }
      }
    }
    new BackendManager().updateDataUpdatesPPL(people);
    Navigator.pop(context, true);
  }

  @override
  Widget build(BuildContext context) {
    new BackendManager().setContext(_scaffoldKey, null, null);
    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        title: new Text("${_evt.id == -1 ? "Add" : "Edit"} Event ${_evt.name.trim()}"),
        actions: (_repeating
                ? <Widget>[
                    PopupMenuButton<Seleccion>(
                      onSelected: (Seleccion result) async {
                        List<CommServEventDay> eDays =
                            _evt.eventDays.map((d) => d.clone()).toList();
                        List<int> effectiveIndices = <int>[];
                        int leftBound =
                            eDays.indexWhere((ed) => ed.deleted == false);
                        int rightBound =
                            eDays.lastIndexWhere((ed) => ed.deleted == false);
                        for (int i = 0; i < eDays.length; i++) {
                          if (!eDays[i].deleted &&
                              eDays[i].startTimeMS <
                                  eDays[leftBound].startTimeMS &&
                              eDays[i].endTimeMS < eDays[leftBound].endTimeMS) {
                            leftBound = i;
                          }
                          if (!eDays[i].deleted &&
                              eDays[i].startTimeMS >
                                  eDays[rightBound].startTimeMS &&
                              eDays[i].endTimeMS >
                                  eDays[rightBound].endTimeMS) {
                            rightBound = i;
                          }
                        }
                        for (int i = 0; i < eDays.length; i++) {
                          if (eDays[i].startTimeMS >
                                  eDays[leftBound].startTimeMS &&
                              eDays[i].endTimeMS > eDays[leftBound].endTimeMS &&
                              eDays[i].startTimeMS <
                                  eDays[rightBound].startTimeMS &&
                              eDays[i].endTimeMS <
                                  eDays[rightBound].endTimeMS) {
                            effectiveIndices.add(i);
                          }
                        }
                        switch (result) {
                          case Seleccion.all:
                            for (int i = 0; i < eDays.length; i++) {
                              if (effectiveIndices.contains(i)) {
                                eDays[i] = eDays[i].clone(Ndeleted: false);
                              }
                            }
                            break;
                          case Seleccion.none:
                            for (int i = 0; i < eDays.length; i++) {
                              if (effectiveIndices.contains(i)) {
                                eDays[i] = eDays[i].clone(Ndeleted: true);
                              }
                            }
                            break;
                          case Seleccion.invert:
                            for (int i = 0; i < eDays.length; i++) {
                              if (effectiveIndices.contains(i)) {
                                eDays[i] =
                                    eDays[i].clone(Ndeleted: !eDays[i].deleted);
                              }
                            }
                            break;
                        }
                        setState(() {
                          _evt = _evt.clone(
                            NeventDays: eDays,
                          );
                        });
                      },
                      itemBuilder: (BuildContext context) =>
                          <PopupMenuEntry<Seleccion>>[
                            const PopupMenuItem<Seleccion>(
                              value: Seleccion.all,
                              child: const Text('Select All Days'),
                            ),
                            const PopupMenuItem<Seleccion>(
                              value: Seleccion.none,
                              child: const Text('Deselect All Days'),
                            ),
                            const PopupMenuItem<Seleccion>(
                              value: Seleccion.invert,
                              child: const Text('Invert Selected Days'),
                            ),
                          ],
                    ),
                  ]
                : <Widget>[]) +
            <Widget>[
              new IconButton(icon: new Icon(Icons.save), onPressed: _save),
            ],
      ),
      body: _buildEditEventView(),
    );
  }

  Widget _buildEditEventView() {
    return new ListView(
      padding:
          new EdgeInsets.only(left: 16.0, right: 16.0, top: 8.0, bottom: 8.0),
      children: <Widget>[
            new TextField(
              maxLines: 1,
              keyboardType: TextInputType.text,
              controller: _nameController,
              decoration: new InputDecoration(hintText: "Event Name"),
            ),
            new TextField(
              maxLines: null,
              keyboardType: TextInputType.multiline,
              controller: _descController,
              decoration: new InputDecoration(hintText: "Event Description"),
            ),
          ] +
          _dayPicker() +
          [
            new ListTile(
              leading: const Icon(Icons.timer),
              title: new Text(
                  "${_timeFormatter.format(_evt.eventDays[0].startTime)}"),
              subtitle: new Text("Start Time"),
              onTap: () async {
                final n = await showTimePicker(
                  context: context,
                  initialTime:
                      new TimeOfDay.fromDateTime(_evt.eventDays[0].startTime),
                );
                if (n == null) return;
                if (!new DateTime(2000, 1, 1, n.hour, n.minute).isBefore(
                    new DateTime(2000, 1, 1, _evt.eventDays[0].endTime.hour,
                        _evt.eventDays[0].endTime.minute))) return;
                final times = _evt.eventDays
                    .map((e) => e.clone(
                          NstartTimeMS: new DateTime(
                                  e.startTime.year,
                                  e.startTime.month,
                                  e.startTime.day,
                                  n.hour,
                                  n.minute,
                                  e.startTime.second,
                                  e.startTime.millisecond,
                                  e.startTime.microsecond)
                              .millisecondsSinceEpoch,
                        ))
                    .toList();
                setState(() => _evt = _evt.clone(
                      NeventDays: times,
                    ));
              },
            ),
            new ListTile(
              leading: const Icon(Icons.timer),
              title: new Text(
                  "${_timeFormatter.format(_evt.eventDays[0].endTime)}"),
              subtitle: new Text("End Time"),
              onTap: () async {
                final n = await showTimePicker(
                  context: context,
                  initialTime:
                      new TimeOfDay.fromDateTime(_evt.eventDays[0].endTime),
                );
                if (n == null) return;
                if (!new DateTime(2000, 1, 1, n.hour, n.minute).isAfter(
                    new DateTime(2000, 1, 1, _evt.eventDays[0].startTime.hour,
                        _evt.eventDays[0].startTime.minute))) return;
                final times = _evt.eventDays
                    .map((e) => e.clone(
                          NendTimeMS: new DateTime(
                                  e.endTime.year,
                                  e.endTime.month,
                                  e.endTime.day,
                                  n.hour,
                                  n.minute,
                                  e.endTime.second,
                                  e.endTime.millisecond,
                                  e.endTime.microsecond)
                              .millisecondsSinceEpoch,
                        ))
                    .toList();
                setState(() => _evt = _evt.clone(
                      NeventDays: times,
                    ));
              },
            ),
            new Divider(),
            new CheckboxListTile(
              value: _evt.limit != -1,
              onChanged: (bool newLimited) {
                setState(() {
                  if (newLimited) {
                    _evt = _evt.clone(Nlimit: 1);
                  } else {
                    _evt = _evt.clone(Nlimit: -1);
                  }
                });
              },
              title: new Text("Limit Signup"),
            ),
            _evt.limit != -1
                ? new Row(children: <Widget>[
                    new Expanded(
                      child:
                          new Text("${_evt.limit.toString()} person maximum"),
                    ),
                    new IconButton(
                        icon: new Icon(Icons.edit),
                        onPressed: _showLimitPicker),
                  ])
                : new NullContainer(),
            new ListTile(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (BuildContext newContext) {
                      TextEditingController cont = new TextEditingController();
                      return new AlertDialog(
                        title: new Text("Add Form"),
                        content: new TextField(
                          controller: cont,
                          decoration: new InputDecoration(hintText: "Form URL"),
                        ),
                        actions: <Widget>[
                          new FlatButton(
                            child: new Text('Cancel'),
                            onPressed: () {
                              Navigator.of(newContext).pop();
                            },
                          ),
                          new FlatButton(
                            child: new Text('Add Form'),
                            onPressed: () {
                              Navigator.of(newContext).pop();
                              setState(() => _evt.files.add(cont.text));
                            },
                          ),
                        ],
                      );
                    });
              },
              title: new Text("Add Form"),
            ),
          ] +
          _getForms() +
          <Widget>[
            new Divider(),
            new ListTile(
              leading: new Icon(Icons.map),
              title: new Text("Set Parking Location"),
              onTap: _showMap,
            ),
            new TextField(
              maxLines: null,
              keyboardType: TextInputType.multiline,
              controller: _parkController,
              decoration: new InputDecoration(hintText: "Parking Info"),
            ),
            _evt.id == -1 ? new NullContainer() : new Divider(),
            _evt.id == -1
                ? new NullContainer()
                : new FlatButton(
                    onPressed: () {
                      showDialog(
                        context: context,
                        barrierDismissible: false,
                        builder: (newContext) {
                          return new AlertDialog(
                            content: new Text(
                                "Are you sure you want to delete ${_evt.name.trim()}?"),
                            actions: <Widget>[
                              new FlatButton(
                                  child: Text('Cancel'),
                                  onPressed: () {
                                    Navigator.of(newContext).pop();
                                  }),
                              new FlatButton(
                                  child: new Text("Delete"),
                                  onPressed: () {
                                    Navigator.of(newContext).pop();
                                    _evt = _evt.clone(
                                        NeventDays: _evt.eventDays
                                            .map((CommServEventDay d) =>
                                                d.clone(Ndeleted: true))
                                            .toList());
                                    new BackendManager()
                                        .updateDataUpdatesE(_evt);
                                    _save();
                                  }),
                            ],
                          );
                        },
                      );
                    },
                    child: new Text(
                      "Delete Event",
                      style: new TextStyle(color: Colors.red),
                    )),
          ],
    );
  }

  bool _repeating;

  DateTime get _rangeStart {
    int smallest = _evt.eventDays
        .firstWhere((CommServEventDay ed) => !ed.deleted)
        .startTimeMS;
    _evt.eventDays.forEach((CommServEventDay d) {
      if (d.startTimeMS < smallest && !d.deleted) {
        smallest = d.startTimeMS;
      }
    });
    return new DateTime.fromMillisecondsSinceEpoch(smallest);
  }

  DateTime get _rangeEnd {
    int biggest = _evt.eventDays
        .firstWhere((CommServEventDay ed) => !ed.deleted)
        .endTimeMS;
    _evt.eventDays.forEach((CommServEventDay d) {
      if (d.endTimeMS > biggest && !d.deleted) {
        biggest = d.endTimeMS;
      }
    });
    return new DateTime.fromMillisecondsSinceEpoch(biggest);
  }

  DateTime _normal(DateTime input) {
    return new DateTime(input.year, input.month, input.day);
  }

  final _formatter = new DateFormat("MEd");

  Widget _getDayList() {
    return new Container(
      child: ListView.builder(
        itemCount:
            _normal(_rangeEnd).difference(_normal(_rangeStart)).inDays + 1,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          DateTime d = _rangeStart.add(new Duration(days: index));
          DateTime e = _rangeEnd.subtract(new Duration(
              days: _normal(_rangeEnd).difference(_normal(_rangeStart)).inDays -
                  index));
          int csedI = _evt.eventDays.indexWhere(
              (CommServEventDay ed) => ed.startTime.isAtSameMomentAs(d));
          //for (int i = 0; i < _evt.eventDays.length; i++) {
          //print(_evt.eventDays[i]);
          //}
          assert(csedI != -1, "wow we stupid on a hot $index with a $d");
          return Padding(
              padding: new EdgeInsets.symmetric(horizontal: 4.0),
              child: Column(
                children: <Widget>[
                  new Text(
                    _formatter.format(d),
                    textAlign: TextAlign.center,
                  ),
                  new Checkbox(
                      value: !_evt.eventDays[csedI].deleted,
                      onChanged: (bool newVal) {
                        List<CommServEventDay> n = new List<CommServEventDay>();
                        for (int i = 0; i < _evt.eventDays.length; i++) {
                          if (i != csedI) {
                            n.add(_evt.eventDays[i].clone());
                          } else {
                            n.add(_evt.eventDays[i].clone(Ndeleted: !newVal));
                          }
                        }
                        setState(() => _evt = _evt.clone(NeventDays: n));
                      }),
                ],
              ));
        },
        scrollDirection: Axis.horizontal,
      ),
      height: 56.0,
    );
  }

  List<Widget> _dayPicker() {
    Widget button = new CheckboxListTile(
        value: _repeating,
        onChanged: (bool newVal) {
          setState(() {
            _repeating = newVal;
            if (!_repeating) {
              _evt = _evt.clone(
                  NeventDays: [_evt.eventDays[0].clone(Ndeleted: false)]);
            }
          });
        },
        title: new Text("Repeating Event"));

    List<Widget> ret = new List<Widget>();
    if (!_repeating) {
      ret.add(
        new ListTile(
          leading: const Icon(Icons.calendar_today),
          title: new Text(_dayFormatter.format(_evt.eventDays[0].startTime)),
          subtitle: new Text("Event Day"),
          onTap: () async {
            final n = await showDatePicker(
                context: context,
                initialDate: _evt.eventDays[0].startTime,
                firstDate: new DateTime(new DateTime.now().year - 5),
                lastDate: new DateTime(new DateTime.now().year + 5));
            if (n == null) return;
            final st = _evt.eventDays[0].startTime;
            final et = _evt.eventDays[0].endTime;
            setState(() => _evt = _evt.clone(NeventDays: [
                  _evt.eventDays[0].clone(
                    NstartTimeMS: new DateTime(
                            n.year,
                            n.month,
                            n.day,
                            st.hour,
                            st.minute,
                            st.second,
                            st.millisecond,
                            st.microsecond)
                        .millisecondsSinceEpoch,
                    NendTimeMS: new DateTime(
                            n.year,
                            n.month,
                            n.day,
                            et.hour,
                            et.minute,
                            et.second,
                            et.millisecond,
                            et.microsecond)
                        .millisecondsSinceEpoch,
                  )
                ]));
          },
        ),
      );
    } else {
      ret.add(
        new ListTile(
          leading: const Icon(Icons.calendar_today),
          title: new Text(_dayFormatter.format(_rangeStart)),
          subtitle: new Text("Range Start"),
          onTap: () async {
            final n = await showDatePicker(
              context: context,
              initialDate: _rangeStart,
              firstDate: new DateTime(new DateTime.now().year - 5),
              lastDate: _rangeEnd,
            );
            if (n == null) return;
            DateTime newStart = new DateTime(
                n.year,
                n.month,
                n.day,
                _rangeStart.hour,
                _rangeStart.minute,
                n.second,
                n.millisecond,
                n.microsecond);
            setState(() {
              final List<int> indices = new List<int>();
              int upper =
                  _normal(_rangeEnd).difference(_normal(newStart)).inDays;

              for (int i = 0; i <= upper; i++) {
                DateTime s = newStart.add(new Duration(days: i));
                DateTime e = _rangeEnd.subtract(new Duration(days: upper - i));
                int csedI = _evt.eventDays.indexWhere(
                    (CommServEventDay ed) => ed.startTime.isAtSameMomentAs(s));
                if (csedI == -1) {
                  csedI = _evt.eventDays.length;
                  _evt = _evt.clone(
                      NeventDays: _evt.eventDays
                        ..add(new CommServEventDay(
                            startTimeMS: s.millisecondsSinceEpoch,
                            endTimeMS: e.millisecondsSinceEpoch,
                            deleted: false)));
                } else if (_evt.eventDays[csedI].deleted &&
                    !s.isAfter(_rangeStart)) {
                  final List<CommServEventDay> newDays =
                      new List<CommServEventDay>();
                  for (int j = 0; j < _evt.eventDays.length; j++) {
                    if (j == csedI) {
                      newDays.add(_evt.eventDays[j].clone(Ndeleted: false));
                    } else {
                      newDays.add(_evt.eventDays[j].clone());
                    }
                  }
                  _evt = _evt.clone(NeventDays: newDays);
                }
                indices.add(csedI);
              }
              for (int i = 0; i < _evt.eventDays.length; i++) {
                if (!indices.contains(i)) {
                  final c = _evt.eventDays;
                  c[i] = c[i].clone(Ndeleted: true);
                  _evt = _evt.clone(NeventDays: c);
                }
              }
            });
          },
        ),
      );
      ret.add(
        new ListTile(
          leading: const Icon(Icons.calendar_today),
          title: new Text(_dayFormatter.format(_rangeEnd)),
          subtitle: new Text("Range End"),
          onTap: () async {
            final n = await showDatePicker(
              context: context,
              initialDate: _rangeEnd,
              firstDate: _rangeStart,
              lastDate: new DateTime(new DateTime.now().year + 5),
            );
            if (n == null) return;
            DateTime newEnd = new DateTime(
                n.year,
                n.month,
                n.day,
                _rangeEnd.hour,
                _rangeEnd.minute,
                n.second,
                n.millisecond,
                n.microsecond);
            setState(() {
              final List<int> indices = new List<int>();
              int upper =
                  _normal(newEnd).difference(_normal(_rangeStart)).inDays;

              for (int i = 0; i <= upper; i++) {
                DateTime s = _rangeStart.add(new Duration(days: i));
                DateTime e = newEnd.subtract(new Duration(days: upper - i));
                int csedI = _evt.eventDays.indexWhere(
                    (CommServEventDay ed) => ed.startTime.isAtSameMomentAs(s));
                if (csedI == -1) {
                  csedI = _evt.eventDays.length;
                  _evt = _evt.clone(
                      NeventDays: _evt.eventDays
                        ..add(new CommServEventDay(
                            startTimeMS: s.millisecondsSinceEpoch,
                            endTimeMS: e.millisecondsSinceEpoch,
                            deleted: false)));
                } else if (_evt.eventDays[csedI].deleted &&
                    !e.isBefore(_rangeEnd)) {
                  final List<CommServEventDay> newDays =
                      new List<CommServEventDay>();
                  for (int j = 0; j < _evt.eventDays.length; j++) {
                    if (j == csedI) {
                      newDays.add(_evt.eventDays[j].clone(Ndeleted: false));
                    } else {
                      newDays.add(_evt.eventDays[j].clone());
                    }
                  }
                  _evt = _evt.clone(NeventDays: newDays);
                }
                indices.add(csedI);
              }
              for (int i = 0; i < _evt.eventDays.length; i++) {
                if (!indices.contains(i)) {
                  final c = _evt.eventDays;
                  c[i] = c[i].clone(Ndeleted: true);
                  _evt = _evt.clone(NeventDays: c);
                }
              }
            });
          },
        ),
      );
      ret.add(_getDayList());
    }
    return [button] + ret;
  }

  void _showLimitPicker() {
    int maxIndex = 0;
    for (var i = 0; i < _evt.eventDays.length; i++) {
      if (_evt.getRoster(new BackendManager().data.people, i).length >
          _evt.getRoster(new BackendManager().data.people, maxIndex).length) {
        maxIndex = i;
      }
    }
    showDialog<int>(
        context: context,
        builder: (BuildContext context) {
          return new NumberPickerDialog.integer(
            minValue: _evt
                .getRoster(new BackendManager().data.people, maxIndex)
                .length,
            maxValue: 9999,
            title: new Text("Maximum attendees"),
            initialIntegerValue: _evt.limit,
          );
        }).then((int value) {
      if (value != null) {
        setState(() {
          _evt = _evt.clone(Nlimit: value);
        });
      }
    });
  }

  void _showMap() {
    _tempLoc = new mv.Location(
        _evt.parkingLocation.latitude, _evt.parkingLocation.longitude);
    mapView.show(
        new mv.MapOptions(
            mapViewType: mv.MapViewType.normal,
            showUserLocation: true,
            initialCameraPosition: new mv.CameraPosition(
                new mv.Location(_evt.parkingLocation.latitude,
                    _evt.parkingLocation.longitude),
                13.0),
            title: "Select ${_evt.name.trim()} Parking Location"),
        toolbarActions: [
          new mv.ToolbarAction("Close", 0),
          new mv.ToolbarAction("Save", 1)
        ]);

    compositeSubscription.add(mapView.onMapReady.listen((_) {
      mapView.setMarkers([
        new mv.Marker("1", "${_evt.name.trim()} Parking",
            _evt.parkingLocation.latitude, _evt.parkingLocation.longitude,
            draggable: true)
      ]);
    }));

    compositeSubscription.add(mapView.onToolbarAction.listen((id) {
      _handleDismiss();
      if (id == 1) {
        _evt = _evt.clone(
            NparkingLocation: new Location(
                latitude: _tempLoc.latitude, longitude: _tempLoc.longitude));
      }
    }));
    compositeSubscription.add(mapView.onAnnotationDragEnd.listen((markerMap) {
      var marker = markerMap.keys.first;
      var location = markerMap[marker]; // The updated position of the marker.
      print("Annotation ${marker.id} moved to ${location.latitude} , ${location
          .longitude}");
      _tempLoc = location;
    }));
  }

  void _handleDismiss() async {
    /*double zoomLevel = await mapView.zoomLevel;
    mv.Location centerLocation = await mapView.centerLocation;
    List<mv.Marker> visibleAnnotations = await mapView.visibleAnnotations;
    print("Zoom Level: $zoomLevel");
    print("Center: $centerLocation");
    print("Visible Annotation Count: ${visibleAnnotations.length}");
    var uri = await staticMapProvider.getImageUriFromMap(mapView,
        width: 900, height: 400);
    setState(() => staticMapUri = uri);*/
    mapView.dismiss();
    compositeSubscription.cancel();
  }

  List<Widget> _getForms() {
    List<Widget> ret = new List<Widget>();
    for (int i = 0; i < _evt.files.length; i++) {
      ret.add(new Row(children: <Widget>[
        new Expanded(
          child: new Text(_evt.files[i]),
        ),
        new IconButton(
          icon: new Icon(Icons.open_in_new),
          onPressed: () async {
            final url = _evt.files[i];
            if (await canLaunch(url)) {
              await launch(url);
            } else {
              throw Exception("bad2 $url");
            }
          },
        ),
        new IconButton(
            icon: new Icon(Icons.delete),
            onPressed: () => setState(() => _evt.files.removeAt(i))),
      ]));
    }
    return ret;
  }
}
