import 'package:flutter/material.dart';
import 'package:map_view/map_view.dart' as mv;
import 'package:url_launcher/url_launcher.dart';

import 'BackendManager.dart';
import 'CommServEvent.dart';
import 'Location.dart';

class ParkingView extends StatefulWidget {
  final int _evt;
  @override
  createState() => new ParkingViewState(_evt);
  ParkingView(this._evt, {Key key}) : super(key: key);
}

class ParkingViewState extends State<ParkingView> {
  static mv.StaticMapProvider _mapProv =
      new mv.StaticMapProvider(BackendManager.MAP_KEY);

  final int _evtID;
  CommServEvent get _evt => new BackendManager()
      .data
      .events
      .firstWhere((CommServEvent e) => e.id == _evtID);
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  ParkingViewState(this._evtID) {
    print(this._evtID);
  }

  var MAP_HEIGHT = 300;

  @override
  Widget build(BuildContext context) {
    MAP_HEIGHT = MediaQuery.of(context).size.width.toInt();
    new BackendManager()
        .setContext(_scaffoldKey, _refreshIndicatorKey, setState);
    return new Scaffold(
        key: _scaffoldKey,
        appBar: new AppBar(
          title: new Text("${_evt.name.trim()} Parking"),
          //leading: new IconButton(
          //icon: new Icon(Icons.arrow_back),
          //onPressed: () => Navigator.pop(context)),
        ),
        body: _buildParkingView());
  }

  Widget _buildParkingView() {
    return new RefreshIndicator(
        child: new ListView(children: <Widget>[
          new ListTile(
            leading: new Icon(Icons.info),
            title: new Text(_evt.parkingInfo.trim()),
            subtitle: new Text("Parking Info"),
          ),
          new Divider(),
          new InkWell(
              onTap: _openMap,
              child: new ListView(
                shrinkWrap: true,
                children: <Widget>[
                  new ListTile(
                      title: new Text(
                          "Tap below to open the location with a map app.")),
                  new Container(
                      height: MAP_HEIGHT.toDouble(),
                      width: MAP_HEIGHT.toDouble(),
                      child: new Image.network(
                        getMapURI(_evt.parkingLocation, MAP_HEIGHT, MAP_HEIGHT)
                            .toString(),
                      ))
                ],
              )),
        ]),
        key: _refreshIndicatorKey,
        onRefresh: new BackendManager().onRefresh);
  }

  Uri getMapURI(Location loc, int width, int height) {
    var markers = <mv.Marker>[
      new mv.Marker("", "", loc.latitude, loc.longitude, color: Colors.red)
    ];
    return _mapProv.getStaticUriWithMarkers(markers,
        width: width,
        height: height,
        maptype: mv.StaticMapViewType.roadmap,
        center: new mv.Location(loc.latitude, loc.longitude));
  }

  void _openMap() async {
    final url =
        'https://www.google.com/maps/search/?api=1&query=${_evt.parkingLocation.latitude},${_evt.parkingLocation.longitude}';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw Exception("bad");
    }
  }
}
